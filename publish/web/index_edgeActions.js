
(function($,Edge,compId){var Composition=Edge.Composition,Symbol=Edge.Symbol;
//Edge symbol: 'stage'
(function(symbolName){$("#Stage").css("margin","auto")
Symbol.bindTriggerAction(compId,symbolName,"Default Timeline",0,function(sym,e){sym.stop();});
//Edge binding end
Symbol.bindElementAction(compId,symbolName,"${_btn_help}","click",function(sym,e){sym.getSymbol("HelpAnim").play();});
//Edge binding end
Symbol.bindElementAction(compId,symbolName,"${_btn_resources}","click",function(sym,e){alert("resources pressed");});
//Edge binding end
Symbol.bindElementAction(compId,symbolName,"${_btn_transcript}","click",function(sym,e){var transcript_txt=sym.getSymbol("TranscriptAnim").getSymbol("Transcript").$("content_txt");transcript_txt.text("Edge set text");sym.getSymbol("TranscriptAnim").play();});
//Edge binding end
Symbol.bindElementAction(compId,symbolName,"${_Stage}","mouseup",function(sym,e){sym.setVariable("dragging",false);console.log("Mouse released");});
//Edge binding end
Symbol.bindElementAction(compId,symbolName,"${_scrollbar}","mousedown",function(sym,e){console.log("_scrollbar clicked");sym.setVariable("dragging",true);var thumb=sym.$("scrollbar");var offset=thumb.offset();sym.setVariable("relY",e.pageY-offset.top);});
//Edge binding end
Symbol.bindElementAction(compId,symbolName,"${_Stage}","mousemove",function(sym,e){var dragging=sym.getVariable("dragging");if(dragging){var thumb=sym.$("scrollbar")
var relY=sym.getVariable("relY");var diffY=e.pageY-relY-100;if(diffY<34)diffY=34;if(diffY>471)diffY=471;thumb.css('top',(diffY)+'px');console.log("Moving the scroll bar thumb "+diffY);}});
//Edge binding end
Symbol.bindElementAction(compId,symbolName,"${_updown}","mousedown",function(sym,e){var btn=sym.$("updown");var offset=btn.offset();var up=((e.pageY-offset.top)<31)
startScroll(up,sym.$("scrollbar"));});
//Edge binding end
Symbol.bindElementAction(compId,symbolName,"${_updown}","mouseup",function(sym,e){endScroll();});
//Edge binding end
Symbol.bindSymbolAction(compId,symbolName,"creationComplete",function(sym,e){var slides=[];var slideID;var slideAnimID;$.getJSON('slides.txt',function(data){console.log("getJSON returned");slides=data;slideID=0;setTimeout(startShow,100);});function startShow(){console.log("startShow");var content1=sym.$("Content1");var content2=sym.$("Content2");content1.css("top",0);content2.css("top",1000);initSlide(1);setTimeout(nextSlide,3000);}
function initSlide(id){var slide=slides[slideID];var str="Content"+id;var content=sym.getSymbol(str);content.setVariable("slide",slide);console.log("initSlide "+id+" name:"+str+" content:"+content+" type:"+slide.type);switch(slide.type){case 1:content.stop("TitleFrm");break;case 2:content.stop("Animation");break;case 3:content.stop("Question");break;case 4:content.stop("Coaching");break;case 5:content.stop("Map");break;case 6:content.stop("VideoFrm");break;}
return content;}
function nextSlide(){if(slides.length<=slideID||slideID<0)return;slideID++;var content1=sym.$("Content1");var content2=sym.$("Content2");var id=(slideID%2)+1;var top;initSlide(id);if(id==1){content1.animate({top:0,opacity:1},1000,function(){console.log("animate finished");});content1.css('top',1000);top=parseInt(content2.css('top'))-1000;content2.animate({top:top,opacity:0},1000);}else{content2.animate({top:0,opacity:1},1000,function(){console.log("animate finished");});content2.css('top',1000);top=parseInt(content1.css('top'))-1000;content1.animate({top:top,opacity:0},1000);}
content2.hide();setTimeout(nextSlide,3000);}
function contentAnim(){var inc=30;var id=(slideID%2)+1;var content1=sym.$("Content1");var content2=sym.$("Content2");var top1=parseInt(content1.css('top'));var top2=parseInt(content2.css('top'));var newSlide,oldSlide,top;if(id==1){newSlide=content1;oldSlide=content2;top=top1;}else{newSlide=content2;oldSlide=content1;top=top2;}
console.log("contentAnim "+top+" "+id);if(top>inc){top1-=inc;top2-=inc;content1.css('top',top1);content2.css('top',top2);}else{top=(id==1)?(top1-top):(top2-top);newSlide.css('top',0);oldSlide.css('top',-5000);oldSlide.hide();clearInterval(slideAnimID);setTimeout(nextSlide,3000);}}});
//Edge binding end
})("stage");
//Edge symbol end:'stage'

//=========================================================

//Edge symbol: 'progress'
(function(symbolName){Symbol.bindTriggerAction(compId,symbolName,"Default Timeline",1750,function(sym,e){sym.stop();});
//Edge binding end
Symbol.bindTriggerAction(compId,symbolName,"Default Timeline",0,function(sym,e){sym.stop();});
//Edge binding end
Symbol.bindTriggerAction(compId,symbolName,"Default Timeline",2500,function(sym,e){sym.stop();});
//Edge binding end
Symbol.bindTriggerAction(compId,symbolName,"Default Timeline",3373,function(sym,e){sym.stop();});
//Edge binding end
Symbol.bindTriggerAction(compId,symbolName,"Default Timeline",4386,function(sym,e){sym.stop();});
//Edge binding end
Symbol.bindTriggerAction(compId,symbolName,"Default Timeline",5250,function(sym,e){sym.stop();});
//Edge binding end
})("progress");
//Edge symbol end:'progress'

//=========================================================

//Edge symbol: 'btn_help'
(function(symbolName){Symbol.bindElementAction(compId,symbolName,"${_help}","click",function(sym,e){});
//Edge binding end
})("btn_help");
//Edge symbol end:'btn_help'

//=========================================================

//Edge symbol: 'btn_transcript'
(function(symbolName){Symbol.bindElementAction(compId,symbolName,"${_transcript}","click",function(sym,e){sym.play("Transcript");});
//Edge binding end
})("btn_transcript");
//Edge symbol end:'btn_transcript'

//=========================================================

//Edge symbol: 'btn_resources'
(function(symbolName){Symbol.bindElementAction(compId,symbolName,"${_resources}","click",function(sym,e){});
//Edge binding end
})("btn_resources");
//Edge symbol end:'btn_resources'

//=========================================================

//Edge symbol: 'Transcript'
(function(symbolName){Symbol.bindElementAction(compId,symbolName,"${_close2}","click",function(sym,e){sym.getParentSymbol().play("Out");});
//Edge binding end
})("Transcript");
//Edge symbol end:'Transcript'

//=========================================================

//Edge symbol: 'TranscriptAnim'
(function(symbolName){Symbol.bindTriggerAction(compId,symbolName,"Default Timeline",1088,function(sym,e){sym.stop();});
//Edge binding end
Symbol.bindTriggerAction(compId,symbolName,"Default Timeline",0,function(sym,e){sym.stop();});
//Edge binding end
Symbol.bindTriggerAction(compId,symbolName,"Default Timeline",2117,function(sym,e){sym.stop(0);});
//Edge binding end
})("TranscriptAnim");
//Edge symbol end:'TranscriptAnim'

//=========================================================

//Edge symbol: 'HelpAnim'
(function(symbolName){Symbol.bindElementAction(compId,symbolName,"${_close}","click",function(sym,e){sym.getParentSymbol().play("Out");});
//Edge binding end
})("Help");
//Edge symbol end:'Help'

//=========================================================

//Edge symbol: 'HelpAnim'
(function(symbolName){Symbol.bindTriggerAction(compId,symbolName,"Default Timeline",1000,function(sym,e){sym.stop();});
//Edge binding end
Symbol.bindTriggerAction(compId,symbolName,"Default Timeline",2021,function(sym,e){sym.stop(0);});
//Edge binding end
})("HelpAnim");
//Edge symbol end:'HelpAnim'

//=========================================================

//Edge symbol: 'CoachingScreen'
(function(symbolName){Symbol.bindTriggerAction(compId,symbolName,"Default Timeline",3000,function(sym,e){sym.stop();});
//Edge binding end
Symbol.bindTriggerAction(compId,symbolName,"Default Timeline",0,function(sym,e){sym.play();});
//Edge binding end
})("CoachingScreen");
//Edge symbol end:'CoachingScreen'

//=========================================================

//Edge symbol: 'Question'
(function(symbolName){})("Answer");
//Edge symbol end:'Answer'

//=========================================================

//Edge symbol: 'SetUpScreen_1'
(function(symbolName){Symbol.bindTriggerAction(compId,symbolName,"Default Timeline",4000,function(sym,e){sym.stop();});
//Edge binding end
})("QuestionScreen");
//Edge symbol end:'QuestionScreen'

//=========================================================

//Edge symbol: 'CheckBox'
(function(symbolName){})("CheckBox");
//Edge symbol end:'CheckBox'

//=========================================================

//Edge symbol: 'btn_next'
(function(symbolName){})("btn_next");
//Edge symbol end:'btn_next'

//=========================================================

//Edge symbol: 'SetUpScreen'
(function(symbolName){Symbol.bindTriggerAction(compId,symbolName,"Default Timeline",3500,function(sym,e){sym.stop();});
//Edge binding end
Symbol.bindTriggerAction(compId,symbolName,"Default Timeline",0,function(sym,e){sym.play();});
//Edge binding end
})("SetUpScreen");
//Edge symbol end:'SetUpScreen'

//=========================================================

//Edge symbol: 'btn_next'
(function(symbolName){})("btn_next_1");
//Edge symbol end:'btn_next_1'

//=========================================================

//Edge symbol: 'content1_sym'
(function(symbolName){Symbol.bindTriggerAction(compId,symbolName,"Default Timeline",1000,function(sym,e){sym.stop();});
//Edge binding end
})("content1_sym");
//Edge symbol end:'content1_sym'

//=========================================================

//Edge symbol: 'MapInteractiveScreen'
(function(symbolName){})("MapInteractiveScreen");
//Edge symbol end:'MapInteractiveScreen'

//=========================================================

//Edge symbol: 'SetUpScreen_1'
(function(symbolName){Symbol.bindTriggerAction(compId,symbolName,"Default Timeline",0,function(sym,e){sym.play();});
//Edge binding end
Symbol.bindTriggerAction(compId,symbolName,"Default Timeline",3500,function(sym,e){sym.stop();});
//Edge binding end
})("IntroScreen");
//Edge symbol end:'IntroScreen'

//=========================================================

//Edge symbol: 'Video'
(function(symbolName){Symbol.bindTriggerAction(compId,symbolName,"Default Timeline",0,function(sym,e){var video="video1";var str='<video id="video" width="930" height="560" preload="none" autoplay="autoplay" loop="loop">';str+='<source src="media/'+video+'.mp4" type="video/mp4" />';str+='<source src="media/'+video+'.ogg" type="video/ogg" />';str+='Your browser does not support the video tag.<br />Please upgrade your browser</video>';console.log("Video "+str);sym.$("video").append(str);sym.stop();});
//Edge binding end
})("Video");
//Edge symbol end:'Video'
})(jQuery,AdobeEdge,"EDGE-5190220");var scrollInt=0;var scroll_sym;function startScroll(up,sym){scroll_sym=sym;if(scrollInt!=0)clearInterval(scrollInt);if(up){scrollInt=setInterval(scrollUp,100);}else{scrollInt=setInterval(scrollDown,100);}}
function endScroll(){clearInterval(scrollInt);scrollInt=0;scroll_sym=null;}
function scrollUp(){var position=scroll_sym.position();var diffY=position.top-10;if(diffY<34)diffY=34;scroll_sym.css('top',(diffY)+'px');}
function scrollDown(){var position=scroll_sym.position();var diffY=position.top+10;if(diffY>471)diffY=471;scroll_sym.css('top',(diffY)+'px');}