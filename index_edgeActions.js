/***********************
* Adobe Edge Animate Composition Actions
*
* Edit this file with caution, being careful to preserve 
* function signatures and comments starting with 'Edge' to maintain the 
* ability to interact with these actions from within Adobe Edge Animate
*
***********************/
(function($, Edge, compId){
var Composition = Edge.Composition, Symbol = Edge.Symbol; // aliases for commonly used Edge classes

   //Edge symbol: 'stage'
   (function(symbolName) {
      $("#Stage").css("margin","auto")
      
      
      
      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 0, function(sym, e) {
         sym.stop();
      
      });
      //Edge binding end
      
      Symbol.bindElementAction(compId, symbolName, "${_btn_help}", "click", function(sym, e) {
         var help = sym.$("Help");
         var top = parseInt(help.css('top'));
		 console.log("help_btn " + help + " " + top);
         if (top<100){
         	//Animate out
         	help.animate({top:600}, 1000);
         }else{
         	//Animate in
         	help.animate({top:0}, 1000);
         }

      });
      //Edge binding end
      
      Symbol.bindElementAction(compId, symbolName, "${_btn_resources}", "click", function(sym, e) {
         alert("resources pressed");
      
      });
      //Edge binding end
      
      Symbol.bindElementAction(compId, symbolName, "${_btn_transcript}", "click", function(sym, e) {
         //get the value of a Symbol variable
         //var transcript = sym.getVariable("TranscriptAnim");
         //console.log("btn_transcript pressed " + transcript);
         //transcript.play();
         var transcript_txt = sym.getSymbol("TranscriptAnim").getSymbol("Transcript").$("content_txt");
         transcript_txt.text("Edge set text");
         sym.getSymbol("TranscriptAnim").play();
         
         

      });
      //Edge binding end
      
      Symbol.bindElementAction(compId, symbolName, "${_Stage}", "mouseup", function(sym, e) { 
         // insert code to be run when the mouse button is released
         sym.setVariable("dragging", false);
      	 console.log("Mouse released");
      });
      //Edge binding end
      
      Symbol.bindElementAction(compId, symbolName, "${_scrollbar}", "mousedown", function(sym, e) {
         // insert code for mouse click here
		 console.log("_scrollbar clicked");
         sym.setVariable("dragging", true);
		 var thumb = sym.$("scrollbar");
		 var offset = thumb.offset();
		 sym.setVariable("relY", e.pageY - offset.top);
      });
      //Edge binding end
	  
	  Symbol.bindElementAction(compId, symbolName, "${_Stage}", "mousemove", function(sym, e) {
         // insert code for mouse click here
		 var dragging = sym.getVariable("dragging");
		 //console.log("Mouse move " + dragging);
         if (dragging){
			var thumb = sym.$("scrollbar")
			var relY = sym.getVariable("relY");
            var diffY = e.pageY - relY - 100;
			if (diffY<34) diffY = 34;
			if (diffY>471) diffY = 471;
            thumb.css('top', (diffY)+'px');
		    console.log("Moving the scroll bar thumb " + diffY);
		 }

      });
      //Edge binding end
	  
      Symbol.bindElementAction(compId, symbolName, "${_updown}", "mousedown", function(sym, e) {
         // insert code to be run when the mouse button is down
         var btn = sym.$("updown");
		 var offset = btn.offset();
		 //console.log( "updown click " + (e.pageY - offset.top) );
		 var up = ((e.pageY - offset.top)<31)
		 startScroll(up, sym.$("scrollbar"));
      });
      //Edge binding end
	  
	  Symbol.bindElementAction(compId, symbolName, "${_updown}", "mouseup", function(sym, e) {
		 endScroll();
      });
      //Edge binding end

      Symbol.bindSymbolAction(compId, symbolName, "creationComplete", function(sym, e) {
         // insert code to be run when the symbol is created here 
		 stage_sym = sym;
		         
         $.getJSON('slides.txt', function(data) {
         	console.log("getJSON returned");
         	slides = data;
         	slideID = 0;
         	setTimeout(startShow, 100);
         });
         
		 function startShow(){
         	console.log("startShow");
         	var content1 = sym.$("Content1");
         	content1.css("top", 0);
         	initSlide(1);
         	//setTimeout(nextSlide, 3000);
         }
		 
		 //Not used
         function contentAnim(){
         	var inc = 30;
         
         	var id = (slideID % 2) + 1; 
         	var content1 = sym.$("Content1");
         	var content2 = sym.$("Content2");
         	var top1 = parseInt(content1.css('top'));
         	var top2 = parseInt(content2.css('top'));
         	var newSlide, oldSlide, top;
         	if (id==1){
         		newSlide = content1;
         		oldSlide = content2;
         		top = top1;
         	}else{
         		newSlide = content2;
         		oldSlide = content1;
         		top = top2;	
         	}
         	console.log("contentAnim " + top + " " + id);
         	if (top>inc){
         		top1 -= inc;
         		top2 -= inc;
         		content1.css('top', top1);
         		content2.css('top', top2);
         	}else{
         		//Done
         		top = (id==1) ? (top1 - top) : (top2 - top);
         		newSlide.css('top', 0);
         		oldSlide.css('top', -5000);// Show an Element.
         		//  (sym.$("name") resolves an Edge Animate element name to a DOM
         		//  element that can be used with jQuery)		
         		oldSlide.hide();
         		clearInterval(slideAnimID);
         		setTimeout(nextSlide, 3000);
         	}
         }

      });
      //Edge binding end

   })("stage");
   //Edge symbol end:'stage'

   //=========================================================
   
   //Edge symbol: 'progress'
   (function(symbolName) {   
   
      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 1750, function(sym, e) {
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 0, function(sym, e) {
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 2500, function(sym, e) {
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 3373, function(sym, e) {
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 4386, function(sym, e) {
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 5250, function(sym, e) {
         sym.stop();

      });
      //Edge binding end

   })("progress");
   //Edge symbol end:'progress'

   //=========================================================
   
   //Edge symbol: 'btn_help'
   (function(symbolName) {   
   
      Symbol.bindElementAction(compId, symbolName, "${_help}", "click", function(sym, e) {


      });
         //Edge binding end

   })("btn_help");
   //Edge symbol end:'btn_help'

   //=========================================================
   
   //Edge symbol: 'btn_transcript'
   (function(symbolName) {   
   
      Symbol.bindElementAction(compId, symbolName, "${_transcript}", "click", function(sym, e) {
         sym.play("Transcript");
         

      });
      //Edge binding end

   })("btn_transcript");
   //Edge symbol end:'btn_transcript'

   //=========================================================
   
   //Edge symbol: 'btn_resources'
   (function(symbolName) {   
   
      Symbol.bindElementAction(compId, symbolName, "${_resources}", "click", function(sym, e) {
         

      });
      //Edge binding end

   })("btn_resources");
   //Edge symbol end:'btn_resources'

   //=========================================================
   
   //Edge symbol: 'Transcript'
   (function(symbolName) {   
   
      Symbol.bindElementAction(compId, symbolName, "${_close2}", "click", function(sym, e) {
         sym.getParentSymbol().play("Out");
         

      });
         //Edge binding end

   })("Transcript");
   //Edge symbol end:'Transcript'

   //=========================================================
   
   //Edge symbol: 'TranscriptAnim'
   (function(symbolName) {   
   
      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 1088, function(sym, e) {
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 0, function(sym, e) {
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 2117, function(sym, e) {
         // stop the timeline at the given position (ms or label)
         sym.stop(0);

      });
      //Edge binding end

   })("TranscriptAnim");
   //Edge symbol end:'TranscriptAnim'

   //=========================================================
   
   //Edge symbol: 'HelpAnim'
   (function(symbolName) {   
   
      Symbol.bindElementAction(compId, symbolName, "${_close}", "click", function(sym, e) {
         
         // play the timeline from the given position (ms or label)
         sym.getParentSymbol().play("Out");
         

      });
         //Edge binding end

   })("Help");
   //Edge symbol end:'Help'

   //=========================================================
   
   //Edge symbol: 'HelpAnim'
   (function(symbolName) {   
   
      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 0, function(sym, e) {
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 2021, function(sym, e) {
         sym.stop(0);

      });
      //Edge binding end

   })("HelpAnim");
   //Edge symbol end:'HelpAnim'

   //=========================================================
   
   //Edge symbol: 'CoachingScreen'
   (function(symbolName) {   
   
      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 3000, function(sym, e) {
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 0, function(sym, e) {
         var slide = slides[slideID];
         var coaching = sym.$('text');
         coaching.text(slide.text);
         sym.play();

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_btn_next}", "click", function(sym, e) {
         nextSlide();

      });
      //Edge binding end

   })("CoachingScreen");
   //Edge symbol end:'CoachingScreen'

   //=========================================================
   
   //Edge symbol: 'Question'
   (function(symbolName) {   
   
   })("Answer");
   //Edge symbol end:'Answer'

   //=========================================================
   
   //Edge symbol: 'SetUpScreen_1'
   (function(symbolName) {   
   
      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 4000, function(sym, e) {
         sym.stop();

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_next_btn}", "click", function(sym, e) {
         // insert code for mouse click here
         var btn = sym.$("next_btn");
         btn.animate({opacity:0}, 500);
         var selectedID = sym.getVariable("selectedID");
		 var correctID = sym.getVariable("correctID");
		 
		 console.log("Question checked pressed selectedID:" + selectedID + " correctID:" + correctID);
		 
		 var answer = sym.getSymbol("answer" + correctID);
		 answer.stop("Correct");
		 var checkBox = answer.getSymbol("checkBox");
		 checkBox.stop('Correct');
			 
		 if (selectedID!=correctID){
			 var answer1 = sym.getSymbol("answer" + selectedID);
			 answer1.stop("Wrong");
			 var checkBox1 = answer1.getSymbol("checkBox");
			 checkBox1.stop('Wrong');		 
		 }
		 
         setTimeout(nextSlide, 3000);
      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 2250, function(sym, e) {
         // insert code here
         var btn = sym.$("next_btn");
         btn.css('opacity', 1.0);

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 135, function(sym, e) {
         // insert code here
         var slide = slides[slideID];
         var question = sym.$('question');
         var answers = [];
         for(var i=0; i<4; i++) answers.push(slide.answers[i]);
         var correctAnswer = answers[0];
         for(i=0; i<4; i++){
			var id = Math.floor(Math.random() * answers.length);
         	var sAnswer = answers.splice(id, 1);
         	answers.push(sAnswer[0]);
         }
		 for(i=0; i<answers.length; i++){
			 if (answers[i]==correctAnswer){
				 id = i + 1;
				 break;
			 }
		 }
	
		 sym.setVariable("correctID", id);
         question.text(slide.question);
         
		 for(var i=1; i<=4; i++){
         	var answer_sym = sym.getSymbol('answer' + i);
			answer_sym.stop(0);
			var checkBox = answer_sym.getSymbol("checkBox");
			checkBox.stop(0);
         	var num = answer_sym.$('num');
         	var answer = answer_sym.$('answer');
         	num.text(i);
         	answer.text(answers[i-1]);
         }
		 
		 setProgress(slide.progress);
      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_answer4}", "click", function(sym, e) {
         // insert code for mouse click here
         answerSelected(4, sym);

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_answer3}", "click", function(sym, e) {
         // insert code for mouse click here
         answerSelected(3, sym);

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_answer2}", "click", function(sym, e) {
         answerSelected(2, sym);

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_answer1}", "click", function(sym, e) {
         answerSelected(1, sym);

      });
      //Edge binding end

   })("QuestionScreen");
   //Edge symbol end:'QuestionScreen'

   //=========================================================
   
   //Edge symbol: 'CheckBox'
   (function(symbolName) {   
   
   })("CheckBox");
   //Edge symbol end:'CheckBox'

   //=========================================================
   
   //Edge symbol: 'btn_next'
   (function(symbolName) {   
   
   })("btn_next");
   //Edge symbol end:'btn_next'

   //=========================================================
   
   //Edge symbol: 'SetUpScreen'
   (function(symbolName) {   
   
      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 3500, function(sym, e) {
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 0, function(sym, e) {
         var slide = slides[slideID];
         var text1 = sym.$('text1');
         var photo = sym.$('photo');
         text1.text(slide.text1);
         photo.attr("src", "images/" + slide.image);
         setProgress(slide.progress);
         sym.play();

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_next_btn}", "click", function(sym, e) {
         // insert code for mouse click here
         nextSlide();

      });
      //Edge binding end

   })("SetUpScreen");
   //Edge symbol end:'SetUpScreen'

   //=========================================================
   
   //Edge symbol: 'btn_next'
   (function(symbolName) {   
   
   })("btn_next_1");
   //Edge symbol end:'btn_next_1'

   //=========================================================
   
   //Edge symbol: 'content1_sym'
   (function(symbolName) {   
   
      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 1000, function(sym, e) {
         sym.stop();

      });
      //Edge binding end

   })("content1_sym");
   //Edge symbol end:'content1_sym'

   //=========================================================
   
   //Edge symbol: 'MapInteractiveScreen'
   (function(symbolName) {   
   
      Symbol.bindElementAction(compId, symbolName, "${_btn_next}", "click", function(sym, e) {
         nextSlide()

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 0, function(sym, e) {
	var slide = slides[slideID];
	var message = sym.$('message');
	console.log("Init Map " + slide.text);
	message.text(slide.text);
	var pin = sym.$("pin5");
	pin.drag(function( ev, dd ){
		$( this ).css({
			top: dd.offsetY,
			left: dd.offsetX - 170
		});
	});
		setProgress(slide.progress);
      });
      //Edge binding end

      

      

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 1000, function(sym, e) {
         sym.stop();

      });
      //Edge binding end

   })("MapInteractiveScreen");
   //Edge symbol end:'MapInteractiveScreen'

   //=========================================================
   
   //Edge symbol: 'SetUpScreen_1'
   (function(symbolName) {   
   
      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 0, function(sym, e) {
	 	 var slide = slides[slideID];
		 var image = sym.$("titleImage");
	     console.log("Title type:" + slide.type + " title:" + slide.title + " image:" + slide.image + " duration:" + slide.duration);
		 var title = sym.$('title');
		 title.text(slide.title);
		 $(this.lookupSelector("titleImage")).attr('src', 'images/' + slide.image);
		 //image.attr('src', 'images/' + slide.image);
		 //sym.$("titleImage").attr("src","images/" + slide.image);
		 //image.html("Hello World");
		 setProgress(slide.progress);
		 setTimeout(nextSlide, slide.duration * 1000);
         sym.play();

      });
         //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 3500, function(sym, e) {
         sym.stop();

      });
         //Edge binding end

      })("IntroScreen");
   //Edge symbol end:'IntroScreen'

   //=========================================================
   
   //Edge symbol: 'Video'
   (function(symbolName) {   
   
      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 0, function(sym, e) {
         // insert code here
         var video = "video1";
         var str = '<video id="video" width="930" height="560" preload="none" controls="controls" autoplay="autoplay" loop="loop">';
         str += '<source src="media/' + video + '.mp4" type="video/mp4" />';
         str += '<source src="media/' + video + '.ogg" type="video/ogg; codecs="theora, vorbis"" />';
         str += 'Your browser does not support the video tag.<br />Please upgrade your browser</video>';
         console.log("Video " + str);
         sym.$("video").append(str);
         sym.stop();
         
			setProgress(slide.progress);

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 1136, function(sym, e) {
         sym.stop();
         // insert code here

      });
      //Edge binding end

   })("Video");
   //Edge symbol end:'Video'

   //=========================================================
   
   //Edge symbol: 'dottedline'
   (function(symbolName) {   
   
   })("dottedline");
   //Edge symbol end:'dottedline'

   //=========================================================
   
   //Edge symbol: 'blob'
   (function(symbolName) {   
   
   })("blob");
   //Edge symbol end:'blob'

   //=========================================================
   
   //Edge symbol: 'MapPin'
   (function(symbolName) {   
   
      Symbol.bindElementAction(compId, symbolName, "${_pin}", "mousedown", function(sym, e) {
         var pin = sym.$('map_pin5');
         var org = { top:parseInt(pin.css('top')), left:parseInt(pin.css('left')) };
         sym.setVariable("dragOrigin", org);
         pin.draggable();
      });
         //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_pin}", "mouseup", function(sym, e) {
         var pin = sym.$('map_pin5');
         var org = sym.getVariable("dragOrigin");
         pin.css('top', org.top);
         pin.css('left', org.left);

      });
         //Edge binding end

   })("MapPin");
   //Edge symbol end:'MapPin'

})(jQuery, AdobeEdge, "EDGE-5190220");

var scrollInt = 0;
var scroll_sym;
var stage_sym;
var slides = [];
var slideID;
var slideAnimID;

function startScroll(up, sym){
	scroll_sym = sym;
	if (scrollInt!=0) clearInterval(scrollInt);
	if (up){
		scrollInt = setInterval(scrollUp, 100);
	}else{
		scrollInt = setInterval(scrollDown, 100);
	}
}

function endScroll(){
	clearInterval(scrollInt);
	scrollInt = 0;
	scroll_sym = null;
}

function scrollUp(){
	 var position = scroll_sym.position();
	 var diffY = position.top - 10;
	 if (diffY<34) diffY = 34;
     scroll_sym.css('top', (diffY)+'px');
}

function scrollDown(){
	 var position = scroll_sym.position();
	 var diffY = position.top + 10;
	 if (diffY>471) diffY = 471;
     scroll_sym.css('top', (diffY)+'px');
}
         
function initSlide(id){
	var slide = slides[slideID];
	//So we can access it from within the symbol
	stage_sym.setVariable("slide", slide);
	var str = "Content" + id;
	var content = stage_sym.getSymbol(str);
	var sym;
	//console.log("initSlide " + id + " name:" + str + " content:" + content + " type:" + slide.type);
	switch(slide.type){
	case 1://title
		content.stop("TitleFrm");
		break;
	case 2://animation
		content.stop("Animation");
		break;
	case 3://question
		content.stop("Question");
		sym = content.getSymbol("QuestionScreen");
		sym.play(0);
		break;
	case 4://coaching
		content.stop("Coaching");
		break;
	case 5://map
		content.stop("Map");
		break;
	case 6://video
		content.stop("VideoFrm");
		break;	
	}
	return content;
}

function nextSlide(){
	if (slides.length<=slideID || slideID<0) return;
	slideID++;
	var content1 = stage_sym.$("Content1");
	var id = (slideID % 2) + 1; 
	var top;
	
	content1.animate({top: -800, opacity:0}, 800, function(){ 
		initSlide(1);
		content1.css('top', 800);
		content1.animate({top: 0, opacity:1}, 800, function(){ console.log("animation complete"); });
	});
	
	var link_sym = stage_sym.getSymbol("LinkAnim");
	link_sym.play(0);
	//setTimeout(nextSlide, 3000);
}

function setProgress(id){
	var progress = stage_sym.getSymbol("progress");
	progress.play("prog" + id);
}

function answerSelected( id, sym ){
	for(var i=1; i<=4; i++){
		var answer = sym.getSymbol("answer" + i);
		var checkBox = answer.getSymbol("checkBox");
		if (i==id){
			checkBox.stop("Checked");
		}else{
			checkBox.stop(0);
		}
	}
	console.log("answerSelected " + id);	
	sym.setVariable( "selectedID", id );
}

function initSlideDirect(){
	initSlide(1);
}