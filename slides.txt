[
	{
		"type":1,
		"comment":"Title",
		"title":"Introduction",
		"image":"title1.png",
		"duration":3,
		"progress": 1
	},
	{
		"type":3,
		"comment":"Question",
		"question": "Which Tax service line helps clients incorporate a tax perspective into all their business objectives and decisions?",
		"answers":[
					"Business Tax Services",
					"International Tax",
					"Global Employer Services",
					"Multistate Tax Services"
		],
		"progress": 2
	},
	{
		"type":3,
		"comment":"Question",
		"question": "What service line falls under AERS?",
		"answers":[
					"Financial Operations & Controls Transformation",
					"Multistate Tax",
					"Deloitte Forensics",
					"Mergers & Acquisitions"
		],
		"progress": 2
	},
	{
		"type":4,
		"comment":"Coaching",
		"text": "Financial Operations & Controls Transformation is a service line under AERS. They help clients improve the effectiveness and efficiency of finance and finance-related operations, primarily where linkages to the financial statements and internal controls are significant. They assist clients with managing technology and business risks associated with external contractual arrangements, internal control reporting, and capital markets systems.",
		"progress": 2
	},
	{
		"type":2,
		"comment":"Animation",
		"image":"anim.png",
		"text1":"This will be text on an animation screen",
		"progress": 1
	},	
	{
		"type":5,
		"comment":"Map",
		"text": "This will be some text on the map screen",
		"drops":[
			{ "x": 1, "y":1, "name":"name1" },
			{ "x": 1, "y":1, "name":"name2" },
			{ "x": 1, "y":1, "name":"name3" },
			{ "x": 1, "y":1, "name":"name4" },
			{ "x": 1, "y":1, "name":"name5" },
			{ "x": 1, "y":1, "name":"name6" }
		],
		"progress": 3
	},
	{
		"type":6,
		"comment":"Video",
		"src":"video1",
		"progress": 4
	}
]