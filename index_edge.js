/**
 * Adobe Edge: symbol definitions
 */
(function($, Edge, compId){
//images folder
var im='images/';

var fonts = {};


var resources = [
];
var symbols = {
"stage": {
   version: "1.0.0",
   minimumCompatibleVersion: "0.1.7",
   build: "1.0.0.185",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
         dom: [
         {
            id:'Deloitte_animatedScree-01',
            type:'image',
            rect:['-145px','-201px','1281px','961px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"Deloitte_animatedScree-01.jpg",'0px','0px']
         },
         {
            id:'Rectangle6',
            type:'rect',
            rect:['0px','0px','980px','650px','auto','auto'],
            fill:["rgba(255,255,255,1.00)"],
            stroke:[0,"rgb(0, 0, 0)","none"]
         },
         {
            id:'LinkAnim',
            type:'rect',
            rect:['490','325','auto','auto','auto','auto']
         },
         {
            id:'Content1',
            type:'rect',
            rect:['0','0','auto','auto','auto','auto']
         },
         {
            id:'Help',
            type:'rect',
            rect:['0px','604px','auto','auto','auto','auto']
         },
         {
            id:'RightBar',
            type:'rect',
            rect:['0px','0px','29px','650px','auto','auto'],
            fill:["rgba(200,218,44,1.00)"],
            stroke:[0,"rgb(0, 0, 0)","none"],
            boxShadow:["",3,3,18,0,"rgba(0,0,0,0.65)"]
         },
         {
            id:'LeftBar',
            type:'rect',
            rect:['949px','0px','31px','650px','auto','auto'],
            fill:["rgba(200,218,44,1)"],
            stroke:[0,"rgb(0, 0, 0)","none"],
            boxShadow:["",-3,3,21,0,"rgba(0,0,0,0.65)"]
         },
         {
            id:'BottomPanel',
            type:'rect',
            rect:['0px','589px','980px','61px','auto','auto'],
            fill:["rgba(200,218,44,1)"],
            stroke:[0,"rgb(0, 0, 0)","none"],
            boxShadow:["",3,-3,18,0,"rgba(0,0,0,0.65)"]
         },
         {
            id:'updown',
            type:'image',
            rect:['945px','531px','39px','59px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"updown.png",'0px','0px']
         },
         {
            id:'TextProgress',
            type:'text',
            rect:['21px','604px','auto','auto','auto','auto'],
            text:"PROGRESS",
            align:"left",
            font:['Tahoma, Geneva, sans-serif',21,"rgba(0,0,0,1.00)","600","none","normal"]
         },
         {
            id:'progress',
            type:'rect',
            rect:['123','593','auto','auto','auto','auto']
         },
         {
            id:'top_shadow',
            type:'image',
            rect:['0','-1px','980px','114px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"top_shadow.png",'0px','0px']
         },
         {
            id:'TranscriptAnim',
            type:'rect',
            rect:['771','43','auto','auto','auto','auto']
         },
         {
            id:'TopBar',
            type:'rect',
            rect:['0px','0px','980px','39px','auto','auto'],
            fill:["rgba(63,63,63,1.00)"],
            stroke:[0,"rgba(0,0,0,1)","none"]
         },
         {
            id:'TextKnow',
            type:'text',
            rect:['27px','7px','auto','auto','auto','auto'],
            text:"KNOW THE BUSINESS",
            align:"left",
            font:['Tahoma, Geneva, sans-serif',19,"rgba(255,255,255,1.00)","600","none","normal"]
         },
         {
            id:'btn_resources',
            type:'rect',
            rect:['705','8','auto','auto','auto','auto'],
            cursor:['pointer']
         },
         {
            id:'btn_help',
            type:'rect',
            rect:['812','6','auto','auto','auto','auto'],
            cursor:['pointer']
         },
         {
            id:'btn_transcript',
            type:'rect',
            rect:['870','7','auto','auto','auto','auto'],
            cursor:['pointer']
         },
         {
            id:'scrollbar',
            type:'image',
            rect:['949px','101px','34px','80px','auto','auto'],
            cursor:['pointer'],
            fill:["rgba(0,0,0,0)",im+"scrollbar.png",'0px','0px']
         }],
         symbolInstances: [
         {
            id:'btn_transcript',
            symbolName:'btn_transcript'
         },
         {
            id:'btn_resources',
            symbolName:'btn_resources'
         },
         {
            id:'Help',
            symbolName:'Help'
         },
         {
            id:'btn_help',
            symbolName:'btn_help'
         },
         {
            id:'progress',
            symbolName:'progress'
         },
         {
            id:'LinkAnim',
            symbolName:'dottedline'
         },
         {
            id:'Content1',
            symbolName:'content1_sym'
         },
         {
            id:'TranscriptAnim',
            symbolName:'TranscriptAnim'
         }
         ]
      },
   states: {
      "Base State": {
         "${_BottomPanel}": [
            ["subproperty", "boxShadow.blur", '18px'],
            ["subproperty", "boxShadow.color", 'rgba(0,0,0,0.65)'],
            ["subproperty", "boxShadow.offsetV", '-3px'],
            ["subproperty", "boxShadow.offsetH", '3px'],
            ["style", "width", '980px']
         ],
         "${_RightBar}": [
            ["color", "background-color", 'rgba(200,218,44,1.00)'],
            ["subproperty", "boxShadow.blur", '18px'],
            ["subproperty", "boxShadow.color", 'rgba(0,0,0,0.65)'],
            ["style", "height", '650px'],
            ["subproperty", "boxShadow.offsetV", '3px'],
            ["subproperty", "boxShadow.offsetH", '3px'],
            ["style", "top", '0px']
         ],
         "${_Deloitte_animatedScree-01}": [
            ["style", "top", '-201px'],
            ["style", "left", '-145px']
         ],
         "${_Help}": [
            ["style", "left", '0px'],
            ["style", "top", '604px']
         ],
         "${_TextKnow}": [
            ["style", "top", '7px'],
            ["style", "font-weight", '600'],
            ["color", "color", 'rgba(255,255,255,1.00)'],
            ["style", "font-family", 'Tahoma, Geneva, sans-serif'],
            ["style", "left", '27px'],
            ["style", "font-size", '19px']
         ],
         "${_btn_resources}": [
            ["style", "cursor", 'pointer']
         ],
         "${_updown}": [
            ["style", "top", '531px'],
            ["style", "left", '945px']
         ],
         "${_TextProgress}": [
            ["color", "color", 'rgba(0,0,0,1.00)'],
            ["style", "top", '604px'],
            ["style", "left", '21px'],
            ["style", "font-size", '21px']
         ],
         "${_Rectangle6}": [
            ["style", "height", '650px'],
            ["color", "background-color", 'rgba(255,255,255,1.00)'],
            ["style", "width", '980px']
         ],
         "${_LeftBar}": [
            ["subproperty", "boxShadow.blur", '21px'],
            ["subproperty", "boxShadow.inset", ''],
            ["subproperty", "boxShadow.color", 'rgba(0,0,0,0.65)'],
            ["style", "overflow", 'visible'],
            ["style", "height", '650px'],
            ["subproperty", "boxShadow.offsetV", '3px'],
            ["subproperty", "boxShadow.offsetH", '-3px'],
            ["style", "width", '31px']
         ],
         "${_btn_transcript}": [
            ["style", "cursor", 'pointer']
         ],
         "${_TopBar}": [
            ["color", "background-color", 'rgba(63,63,63,1.00)'],
            ["style", "height", '39px'],
            ["style", "width", '980px']
         ],
         "${_top_shadow}": [
            ["style", "top", '-1px']
         ],
         "${_Stage}": [
            ["color", "background-color", 'rgba(255,255,255,1)'],
            ["style", "width", '980px'],
            ["style", "height", '650px'],
            ["style", "overflow", 'hidden']
         ],
         "${_btn_help}": [
            ["style", "cursor", 'pointer']
         ],
         "${_LinkAnim}": [
            ["style", "top", '0px'],
            ["style", "left", '200px']
         ],
         "${_scrollbar}": [
            ["style", "top", '101px'],
            ["style", "left", '949px'],
            ["style", "cursor", 'pointer']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 0,
         autoPlay: true,
         timeline: [
            { id: "eid278", tween: [ "style", "${_LinkAnim}", "left", '200px', { fromValue: '200px'}], position: 0, duration: 0 },
            { id: "eid277", tween: [ "style", "${_LinkAnim}", "top", '0px', { fromValue: '0px'}], position: 0, duration: 0, easing: "easeOutBounce" },
            { id: "eid136", trigger: [ function executeSymbolFunction(e, data) { this._executeSymbolAction(e, data); }, ['play', '${_Content1}', [] ], ""], position: 0 }         ]
      }
   }
},
"progress": {
   version: "1.0.0",
   minimumCompatibleVersion: "0.1.7",
   build: "1.0.0.185",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      rect: ['5px','-4px','841px','56px','auto','auto'],
      id: 'bottom_menu1',
      type: 'image',
      display: 'none',
      fill: ['rgba(0,0,0,0)','images/bottom_menu1.png','0px','0px']
   },
   {
      rect: ['5px','0px','841px','52px','auto','auto'],
      id: 'bottom_menu2',
      type: 'image',
      display: 'none',
      fill: ['rgba(0,0,0,0)','images/bottom_menu2.png','0px','0px']
   },
   {
      rect: ['5px','-5px','841px','58px','auto','auto'],
      id: 'bottom_menu3',
      type: 'image',
      display: 'none',
      fill: ['rgba(0,0,0,0)','images/bottom_menu3.png','0px','0px']
   },
   {
      rect: ['5px','-6px','841px','61px','auto','auto'],
      id: 'bottom_menu4',
      type: 'image',
      display: 'none',
      fill: ['rgba(0,0,0,0)','images/bottom_menu4.png','0px','0px']
   },
   {
      rect: ['5px','-7px','841px','65px','auto','auto'],
      id: 'bottom_menu5',
      type: 'image',
      display: 'none',
      fill: ['rgba(0,0,0,0)','images/bottom_menu5.png','0px','0px']
   },
   {
      type: 'text',
      rect: ['709px','17px','auto','auto','auto','auto'],
      id: 'Text13',
      text: '5. How we fit in',
      align: 'left',
      font: ['Tahoma, Geneva, sans-serif',10,'rgba(0,0,0,1)','600','none','normal']
   },
   {
      type: 'text',
      rect: ['516px','17px','auto','auto','auto','auto'],
      id: 'Text12',
      text: '4. How Deloitte makes money',
      align: 'left',
      font: ['Tahoma, Geneva, sans-serif',10,'rgba(0,0,0,1)','600','none','normal']
   },
   {
      type: 'text',
      rect: ['339px','17px','auto','auto','auto','auto'],
      id: 'Text11',
      text: '3. Deloitte\'s past and future',
      align: 'left',
      font: ['Tahoma, Geneva, sans-serif',10,'rgba(0,0,0,1)','600','none','normal']
   },
   {
      type: 'text',
      rect: ['128px','17px','auto','auto','auto','auto'],
      id: 'Text10',
      text: '2. What are professional services',
      align: 'left',
      font: ['Tahoma, Geneva, sans-serif',10,'rgba(0,0,0,1)','600','none','normal']
   },
   {
      type: 'text',
      rect: ['29px','17px','auto','auto','auto','auto'],
      id: 'Text9',
      text: '1. Introduction',
      align: 'left',
      font: ['Tahoma, Geneva, sans-serif',10,'rgba(0,0,0,1)','600','none','normal']
   },
   {
      id: 'bottom_menu',
      type: 'image',
      rect: ['5px','-10px','841px','66px','auto','auto'],
      fill: ['rgba(0,0,0,0)','images/bottom_menu.png','0px','0px']
   },
   {
      id: 'triangle',
      type: 'image',
      rect: ['58px','29px','26px','28px','auto','auto'],
      fill: ['rgba(0,0,0,0)','images/triangle.png','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_Text11}": [
            ["style", "top", '17px'],
            ["style", "left", '339px']
         ],
         "${_Text13}": [
            ["style", "top", '17px'],
            ["style", "left", '709px']
         ],
         "${_bottom_menu2}": [
            ["style", "top", '0px'],
            ["style", "left", '5px'],
            ["style", "display", 'none']
         ],
         "${_bottom_menu5}": [
            ["style", "top", '-7px'],
            ["style", "left", '5px'],
            ["style", "display", 'none']
         ],
         "${_Text10}": [
            ["style", "top", '17px'],
            ["style", "left", '128px']
         ],
         "${_bottom_menu1}": [
            ["style", "top", '-4px'],
            ["style", "left", '5px'],
            ["style", "display", 'none']
         ],
         "${_bottom_menu}": [
            ["style", "left", '5px'],
            ["style", "top", '-10px']
         ],
         "${_triangle}": [
            ["style", "top", '29px'],
            ["style", "left", '-258px']
         ],
         "${symbolSelector}": [
            ["style", "height", '48px'],
            ["style", "width", '846px']
         ],
         "${_Text12}": [
            ["style", "top", '17px'],
            ["style", "left", '516px']
         ],
         "${_Text9}": [
            ["style", "top", '17px'],
            ["style", "font-family", 'Tahoma, Geneva, sans-serif'],
            ["style", "left", '29px'],
            ["style", "font-size", '10px']
         ],
         "${_bottom_menu4}": [
            ["style", "top", '-6px'],
            ["style", "left", '5px'],
            ["style", "display", 'none']
         ],
         "${_bottom_menu3}": [
            ["style", "top", '-5px'],
            ["style", "left", '5px'],
            ["style", "display", 'none']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 5250,
         autoPlay: false,
         labels: {
            "prog1": 1000,
            "prog2": 1750,
            "prog3": 2500,
            "prog4": 3373,
            "prog5": 4386
         },
         timeline: [
            { id: "eid24", tween: [ "style", "${_bottom_menu2}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
            { id: "eid25", tween: [ "style", "${_bottom_menu2}", "display", 'block', { fromValue: 'none'}], position: 2000, duration: 0 },
            { id: "eid55", tween: [ "style", "${_triangle}", "left", '-258px', { fromValue: '-258px'}], position: 0, duration: 0, easing: "easeOutQuad" },
            { id: "eid37", tween: [ "style", "${_triangle}", "left", '58px', { fromValue: '-258px'}], position: 1000, duration: 500, easing: "easeInOutQuad" },
            { id: "eid40", tween: [ "style", "${_triangle}", "left", '68px', { fromValue: '58px'}], position: 1500, duration: 83, easing: "easeInOutQuad" },
            { id: "eid41", tween: [ "style", "${_triangle}", "left", '58px', { fromValue: '68px'}], position: 1583, duration: 64, easing: "easeInOutQuad" },
            { id: "eid46", tween: [ "style", "${_triangle}", "left", '58px', { fromValue: '58px'}], position: 1647, duration: 103, easing: "easeInQuad" },
            { id: "eid53", tween: [ "style", "${_triangle}", "left", '208px', { fromValue: '58px'}], position: 1750, duration: 250, easing: "easeOutQuad" },
            { id: "eid50", tween: [ "style", "${_triangle}", "left", '408px', { fromValue: '208px'}], position: 2750, duration: 250, easing: "easeInOutQuad" },
            { id: "eid51", tween: [ "style", "${_triangle}", "left", '598px', { fromValue: '408px'}], position: 3750, duration: 250, easing: "easeInOutQuad" },
            { id: "eid52", tween: [ "style", "${_triangle}", "left", '758px', { fromValue: '598px'}], position: 4750, duration: 250, easing: "easeInOutQuad" },
            { id: "eid23", tween: [ "style", "${_bottom_menu3}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
            { id: "eid26", tween: [ "style", "${_bottom_menu3}", "display", 'block', { fromValue: 'none'}], position: 3000, duration: 0 },
            { id: "eid20", tween: [ "style", "${_bottom_menu1}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
            { id: "eid19", tween: [ "style", "${_bottom_menu1}", "display", 'block', { fromValue: 'none'}], position: 1000, duration: 0 },
            { id: "eid21", tween: [ "style", "${_bottom_menu5}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
            { id: "eid28", tween: [ "style", "${_bottom_menu5}", "display", 'block', { fromValue: 'none'}], position: 5000, duration: 0 },
            { id: "eid22", tween: [ "style", "${_bottom_menu4}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
            { id: "eid27", tween: [ "style", "${_bottom_menu4}", "display", 'block', { fromValue: 'none'}], position: 4000, duration: 0 }         ]
      }
   }
},
"btn_help": {
   version: "1.0.0",
   minimumCompatibleVersion: "0.1.7",
   build: "1.0.0.185",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      rect: ['0px','6px','auto','auto','auto','auto'],
      font: ['Tahoma, Geneva, sans-serif',11,'rgba(255,255,255,1)','600','none','normal'],
      id: 'TextHelp',
      text: 'Help',
      align: 'left',
      type: 'text'
   },
   {
      type: 'image',
      id: 'help',
      rect: ['25px','0px','19px','22px','auto','auto'],
      cursor: ['pointer'],
      fill: ['rgba(0,0,0,0)','images/help.png','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_TextHelp}": [
            ["style", "top", '6px'],
            ["style", "left", '0px']
         ],
         "${_help}": [
            ["style", "top", '0px'],
            ["style", "left", '25px'],
            ["style", "cursor", 'pointer']
         ],
         "${symbolSelector}": [
            ["style", "height", '22px'],
            ["style", "width", '44px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 0,
         autoPlay: true,
         timeline: [
         ]
      }
   }
},
"btn_transcript": {
   version: "1.0.0",
   minimumCompatibleVersion: "0.1.7",
   build: "1.0.0.185",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      type: 'text',
      rect: ['0px','5px','auto','auto','auto','auto'],
      id: 'TextTranscript',
      text: 'Transcript',
      align: 'left',
      font: ['Tahoma, Geneva, sans-serif',11,'rgba(255,255,255,1)','600','none','normal']
   },
   {
      id: 'transcript',
      type: 'image',
      rect: ['59px','0px','24px','26px','auto','auto'],
      fill: ['rgba(0,0,0,0)','images/transcript.png','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_TextTranscript}": [
            ["style", "top", '5px'],
            ["style", "left", '0px']
         ],
         "${_transcript}": [
            ["style", "left", '59px'],
            ["style", "top", '0px']
         ],
         "${symbolSelector}": [
            ["style", "height", '26px'],
            ["style", "width", '83px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 0,
         autoPlay: true,
         timeline: [
         ]
      }
   }
},
"btn_resources": {
   version: "1.0.0",
   minimumCompatibleVersion: "0.1.7",
   build: "1.0.0.185",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      rect: ['1px','4px','auto','auto','auto','auto'],
      font: ['Tahoma, Geneva, sans-serif',11,'rgba(255,255,255,1)','600','none','normal'],
      id: 'TextResources',
      text: 'Resources',
      align: 'left',
      type: 'text'
   },
   {
      type: 'image',
      id: 'resources',
      rect: ['63px','0px','26px','22px','auto','auto'],
      cursor: ['pointer'],
      fill: ['rgba(0,0,0,0)','images/resources.png','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_resources}": [
            ["style", "top", '0px'],
            ["style", "left", '62.5px'],
            ["style", "cursor", 'pointer']
         ],
         "${_TextResources}": [
            ["style", "top", '4px'],
            ["style", "left", '0px'],
            ["style", "font-size", '11px']
         ],
         "${symbolSelector}": [
            ["style", "height", '22px'],
            ["style", "width", '88.5px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 0,
         autoPlay: true,
         timeline: [
         ]
      }
   }
},
"Transcript": {
   version: "1.0.0",
   minimumCompatibleVersion: "0.1.7",
   build: "1.0.0.185",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      type: 'rect',
      id: 'TranscriptPanel',
      stroke: [0,'rgb(0, 0, 0)','none'],
      rect: ['0px','0px','172px','264px','auto','auto'],
      fill: ['rgba(77,77,77,1.00)'],
      c: [
      {
         type: 'rect',
         id: 'Rectangle2',
         stroke: [0,'rgb(0, 0, 0)','none'],
         rect: ['0px','0px','172px','26px','auto','auto'],
         fill: ['rgba(33,33,33,1)']
      },
      {
         type: 'text',
         rect: ['7px','28px','auto','auto','auto','auto'],
         id: 'Text3',
         text: 'Transcript',
         align: 'left',
         font: ['Arial, Helvetica, sans-serif',20,'rgba(255,255,255,1.00)','normal','none','normal']
      },
      {
         type: 'rect',
         id: 'Rectangle3',
         stroke: [0,'rgb(0, 0, 0)','none'],
         rect: ['9px','55px','155px','1px','auto','auto'],
         fill: ['rgba(255,255,255,1.00)']
      },
      {
         type: 'text',
         rect: ['10px','60px','137px','196px','auto','auto'],
         id: 'content_txt',
         text: 'Transcript text goes here',
         align: 'left',
         font: ['Arial, Helvetica, sans-serif',12,'rgba(255,255,255,1)','normal','none','normal']
      },
      {
         type: 'image',
         id: 'close2',
         rect: ['147px','2px','23px','23px','auto','auto'],
         cursor: ['pointer'],
         fill: ['rgba(0,0,0,0)','images/close.png','0px','0px']
      }]
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_content_txt}": [
            ["style", "top", '60px'],
            ["style", "overflow", 'visible'],
            ["style", "height", '196.08332824707px'],
            ["style", "font-size", '12px'],
            ["style", "left", '10px'],
            ["style", "width", '137px']
         ],
         "${_Rectangle2}": [
            ["color", "background-color", 'rgba(33,33,33,1.00)'],
            ["style", "left", '0px'],
            ["style", "top", '0px']
         ],
         "${_TranscriptPanel}": [
            ["style", "display", 'block'],
            ["style", "top", '0px'],
            ["style", "left", '0px'],
            ["color", "background-color", 'rgba(77,77,77,1.00)']
         ],
         "${_Text3}": [
            ["color", "color", 'rgba(255,255,255,1.00)'],
            ["style", "left", '7px'],
            ["style", "top", '28px']
         ],
         "${_close2}": [
            ["style", "top", '2px'],
            ["style", "left", '147px'],
            ["style", "cursor", 'pointer']
         ],
         "${symbolSelector}": [
            ["style", "height", '264px'],
            ["style", "width", '172px']
         ],
         "${_Rectangle3}": [
            ["style", "height", '1px'],
            ["style", "top", '55px'],
            ["style", "left", '9px'],
            ["color", "background-color", 'rgba(255,255,255,1.00)']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 0,
         autoPlay: false,
         timeline: [
            { id: "eid62", tween: [ "style", "${_TranscriptPanel}", "display", 'block', { fromValue: 'block'}], position: 0, duration: 0, easing: "easeInOutQuad" }         ]
      }
   }
},
"TranscriptAnim": {
   version: "1.0.0",
   minimumCompatibleVersion: "0.1.7",
   build: "1.0.0.185",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      id: 'Transcript',
      type: 'rect',
      rect: ['0px','0px','auto','auto','auto','auto']
   }],
   symbolInstances: [
   {
      id: 'Transcript',
      symbolName: 'Transcript'
   }   ]
   },
   states: {
      "Base State": {
         "${symbolSelector}": [
            ["style", "height", '264px'],
            ["style", "width", '172px']
         ],
         "${_Transcript}": [
            ["style", "left", '0px'],
            ["style", "top", '-311px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 2117,
         autoPlay: false,
         labels: {
            "In": 0,
            "Out": 1088
         },
         timeline: [
            { id: "eid73", tween: [ "style", "${_Transcript}", "top", '0px', { fromValue: '-311px'}], position: 0, duration: 1088, easing: "easeInOutBounce" },
            { id: "eid78", tween: [ "style", "${_Transcript}", "top", '-311px', { fromValue: '0px'}], position: 1088, duration: 1029, easing: "easeInOutQuad" }         ]
      }
   }
},
"Help": {
   version: "1.0.0",
   minimumCompatibleVersion: "0.1.7",
   build: "1.0.0.185",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      type: 'rect',
      id: 'Help',
      stroke: [0,'rgb(0, 0, 0)','none'],
      rect: ['0px','0px','980px','650px','auto','auto'],
      fill: ['rgba(231,232,233,1.00)'],
      c: [
      {
         type: 'text',
         rect: ['77px','45px','auto','auto','auto','auto'],
         id: 'TextHelpHeading',
         text: 'Help',
         align: 'left',
         font: ['Arial, Helvetica, sans-serif',104,'rgba(45,163,220,1)','700','none','normal']
      },
      {
         type: 'rect',
         id: 'WhiteLine',
         stroke: [0,'rgb(0, 0, 0)','none'],
         rect: ['327px','159px','585px','3px','auto','auto'],
         fill: ['rgba(255,255,255,1.00)']
      },
      {
         type: 'text',
         rect: ['327','175px','auto','auto','auto','auto'],
         id: 'heading1_txt',
         text: 'Help Topic 1',
         align: 'left',
         font: ['Arial, Helvetica, sans-serif',18,'rgba(38,52,111,1.00)','bold','none','normal']
      },
      {
         type: 'text',
         rect: ['452px','197px','auto','auto','auto','auto'],
         id: 'copy1_txt',
         text: 'Help goes here',
         align: 'left',
         font: ['Arial, Helvetica, sans-serif',13,'rgba(0,0,0,1.00)','400','none','normal']
      },
      {
         type: 'image',
         id: 'close',
         rect: ['327px','136px','23px','23px','auto','auto'],
         cursor: ['pointer'],
         fill: ['rgba(0,0,0,0)','images/close.png','0px','0px']
      }]
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_close}": [
            ["style", "top", '136px'],
            ["style", "left", '327px'],
            ["style", "cursor", 'pointer']
         ],
         "${_heading1_txt}": [
            ["color", "color", 'rgba(38,52,111,1.00)'],
            ["style", "top", '175px'],
            ["style", "font-size", '18px']
         ],
         "${symbolSelector}": [
            ["style", "height", '650px'],
            ["style", "width", '980px']
         ],
         "${_TextHelpHeading}": [
            ["style", "top", '45px'],
            ["style", "font-family", 'Arial, Helvetica, sans-serif'],
            ["color", "color", 'rgba(45,163,220,1.00)'],
            ["style", "font-weight", '700'],
            ["style", "left", '77px'],
            ["style", "font-size", '104px']
         ],
         "${_Help}": [
            ["color", "background-color", 'rgba(231,232,233,1.00)'],
            ["style", "top", '0px'],
            ["style", "height", '650px'],
            ["style", "display", 'block'],
            ["style", "left", '0px'],
            ["style", "width", '980px']
         ],
         "${_WhiteLine}": [
            ["style", "height", '3px'],
            ["color", "background-color", 'rgba(255,255,255,1.00)'],
            ["style", "left", '327px'],
            ["style", "top", '159px']
         ],
         "${_copy1_txt}": [
            ["style", "top", '197px'],
            ["color", "color", 'rgba(0,0,0,1.00)'],
            ["style", "font-weight", '400'],
            ["style", "left", '452px'],
            ["style", "font-size", '13px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 0,
         autoPlay: false,
         timeline: [
            { id: "eid5", tween: [ "style", "${_Help}", "display", 'block', { fromValue: 'block'}], position: 0, duration: 0 }         ]
      }
   }
},
"HelpAnim": {
   version: "1.0.0",
   minimumCompatibleVersion: "0.1.7",
   build: "1.0.0.185",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      id: 'Help',
      type: 'rect',
      rect: ['0px','0px','auto','auto','auto','auto']
   }],
   symbolInstances: [
   {
      id: 'Help',
      symbolName: 'Help'
   }   ]
   },
   states: {
      "Base State": {
         "${symbolSelector}": [
            ["style", "height", '650px'],
            ["style", "width", '980px']
         ],
         "${_Help}": [
            ["style", "left", '0px'],
            ["style", "top", '0px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 2021,
         autoPlay: false,
         timeline: [
         ]
      }
   }
},
"CoachingScreen": {
   version: "1.0.0",
   minimumCompatibleVersion: "0.1.7",
   build: "1.0.0.185",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      rect: ['0px','0px','980px','636px','auto','auto'],
      id: 'Rectangle7',
      stroke: [0,'rgb(0, 0, 0)','none'],
      type: 'rect',
      fill: ['rgba(255,255,255,1.00)']
   },
   {
      transform: [{1:0,0:0},['180deg']],
      id: 'DottedLine22',
      type: 'image',
      rect: ['-242px','-79px','1045px','741px','auto','auto'],
      fill: ['rgba(0,0,0,0)','images/DottedLine2.svg','0px','0px']
   },
   {
      rect: ['254px','150px','721px','337px','auto','auto'],
      id: 'Rectangle6',
      stroke: [0,'rgb(0, 0, 0)','none'],
      type: 'rect',
      fill: ['rgba(151,201,75,1)'],
      c: [
      {
         rect: ['0px','79px','721px','4px','auto','auto'],
         id: 'Rectangle8',
         stroke: [0,'rgb(0, 0, 0)','none'],
         type: 'rect',
         fill: ['rgba(255,255,255,1.00)']
      },
      {
         id: 'CoachingIcon',
         type: 'image',
         rect: ['-455px','-338px','1024px','768px','auto','auto'],
         fill: ['rgba(0,0,0,0)','images/CoachingIcon.svg','0px','0px']
      },
      {
         font: ['Arial, Helvetica, sans-serif',24,'rgba(255,255,255,1)','normal','none','normal'],
         type: 'text',
         id: 'text',
         text: 'Coaching text',
         align: 'left',
         rect: ['35px','97px','665px','223px','auto','auto']
      }]
   },
   {
      font: ['Arial, Helvetica, sans-serif',43,'rgba(38,52,111,1.00)','normal','none','normal'],
      type: 'text',
      id: 'Text12',
      text: 'Coaching',
      align: 'left',
      rect: ['724px','85px','auto','auto','auto','auto']
   },
   {
      rect: ['862px','1052px','auto','auto','auto','auto'],
      id: 'btn_next',
      display: 'none',
      cursor: ['pointer'],
      type: 'rect'
   }],
   symbolInstances: [
   {
      id: 'btn_next',
      symbolName: 'btn_next'
   }   ]
   },
   states: {
      "Base State": {
         "${_Rectangle7}": [
            ["style", "height", '635.66668701172px'],
            ["style", "top", '0px'],
            ["style", "left", '0px'],
            ["style", "width", '980px']
         ],
         "${_CoachingIcon}": [
            ["style", "top", '-338.32px'],
            ["style", "left", '-454.67px']
         ],
         "${_Rectangle6}": [
            ["style", "top", '650.25px'],
            ["style", "left", '254px']
         ],
         "${_Rectangle8}": [
            ["color", "background-color", 'rgba(255,255,255,1.00)'],
            ["style", "top", '78.68px'],
            ["style", "left", '0px'],
            ["style", "width", '721px']
         ],
         "${symbolSelector}": [
            ["style", "height", '635.66668701172px'],
            ["style", "width", '980px']
         ],
         "${_Text12}": [
            ["color", "color", 'rgba(38,52,111,1.00)'],
            ["style", "top", '644.75px'],
            ["style", "left", '724.33px'],
            ["style", "font-size", '43px']
         ],
         "${_DottedLine22}": [
            ["style", "top", '-78.67px'],
            ["transform", "rotateZ", '180deg'],
            ["style", "height", '741.33337402344px'],
            ["style", "left", '-39px'],
            ["style", "width", '1045.3332519531px']
         ],
         "${_text}": [
            ["style", "left", '34.65px'],
            ["style", "top", '97.35px']
         ],
         "${_btn_next}": [
            ["style", "top", '530.85px'],
            ["transform", "scaleY", '1'],
            ["style", "display", 'none'],
            ["transform", "scaleX", '1'],
            ["style", "cursor", 'pointer'],
            ["style", "left", '845px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 3000,
         autoPlay: false,
         timeline: [
            { id: "eid272", tween: [ "style", "${_btn_next}", "left", '996.33px', { fromValue: '845px'}], position: 0, duration: 1980, easing: "easeOutBounce" },
            { id: "eid273", tween: [ "style", "${_btn_next}", "left", '836.05px', { fromValue: '996.33px'}], position: 1980, duration: 520, easing: "easeOutBounce" },
            { id: "eid191", tween: [ "style", "${_DottedLine22}", "left", '-38.79px', { fromValue: '-39px'}], position: 0, duration: 1980, easing: "easeInQuad" },
            { id: "eid212", tween: [ "style", "${_DottedLine22}", "left", '-242.32px', { fromValue: '-38.79px'}], position: 1980, duration: 1020, easing: "easeOutQuad" },
            { id: "eid187", tween: [ "style", "${_Text12}", "top", '84.98px', { fromValue: '644.75px'}], position: 0, duration: 2000, easing: "easeOutBounce" },
            { id: "eid271", tween: [ "style", "${_btn_next}", "display", 'block', { fromValue: 'none'}], position: 1980, duration: 0, easing: "easeOutBounce" },
            { id: "eid287", tween: [ "transform", "${_btn_next}", "scaleX", '1.2', { fromValue: '1'}], position: 2500, duration: 136 },
            { id: "eid288", tween: [ "transform", "${_btn_next}", "scaleX", '1', { fromValue: '1.2'}], position: 2636, duration: 253, easing: "easeOutBounce" },
            { id: "eid189", tween: [ "style", "${_Rectangle6}", "top", '150.33px', { fromValue: '650.25px'}], position: 0, duration: 2500, easing: "easeOutBounce" },
            { id: "eid289", tween: [ "transform", "${_btn_next}", "scaleY", '1.2', { fromValue: '1'}], position: 2500, duration: 136 },
            { id: "eid290", tween: [ "transform", "${_btn_next}", "scaleY", '1', { fromValue: '1.2'}], position: 2636, duration: 253, easing: "easeOutBounce" },
            { id: "eid274", tween: [ "style", "${_btn_next}", "top", '535.86px', { fromValue: '530.85px'}], position: 0, duration: 1980, easing: "easeOutBounce" },
            { id: "eid281", tween: [ "style", "${_btn_next}", "top", '497.82px', { fromValue: '535.86px'}], position: 1980, duration: 520 }         ]
      }
   }
},
"Answer": {
   version: "1.0.0",
   minimumCompatibleVersion: "0.1.7",
   build: "1.0.0.185",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      id: 'QuestionPanelDefault',
      type: 'image',
      rect: ['0px','0px','1024px','768px','auto','auto'],
      fill: ['rgba(0,0,0,0)','images/QuestionPanelDefault.svg','0px','0px']
   },
   {
      rect: ['0','0','1024px','768px','auto','auto'],
      id: 'QuestionPanelCorrect',
      type: 'image',
      display: 'none',
      fill: ['rgba(0,0,0,0)','images/QuestionPanelCorrect.svg','0px','0px']
   },
   {
      rect: ['0','0','1024px','768px','auto','auto'],
      id: 'QuestionPanelWrong',
      type: 'image',
      display: 'none',
      fill: ['rgba(0,0,0,0)','images/QuestionPanelWrong.svg','0px','0px']
   },
   {
      type: 'text',
      rect: ['249px','377px','auto','auto','auto','auto'],
      id: 'Text7',
      text: 'ANSWER',
      align: 'left',
      font: ['Arial, Helvetica, sans-serif',11,'rgba(255,255,255,1)','normal','none','normal']
   },
   {
      type: 'text',
      rect: ['310px','361px','auto','auto','auto','auto'],
      id: 'num',
      text: '1',
      align: 'left',
      font: ['Arial, Helvetica, sans-serif',39,'rgba(255,255,255,1)','normal','none','normal']
   },
   {
      type: 'text',
      rect: ['359px','363px','344px','44px','auto','auto'],
      id: 'answer',
      text: 'Answer',
      align: 'left',
      font: ['Arial, Helvetica, sans-serif',16,'rgba(255,255,255,1)','normal','none','normal']
   },
   {
      id: 'checkBox',
      type: 'rect',
      rect: ['714','365','auto','auto','auto','auto']
   }],
   symbolInstances: [
   {
      id: 'checkBox',
      symbolName: 'CheckBox'
   }   ]
   },
   states: {
      "Base State": {
         "${_QuestionPanelDefault}": [
            ["style", "left", '0px'],
            ["style", "top", '0px']
         ],
         "${_Text7}": [
            ["style", "top", '377px'],
            ["style", "left", '248.67px'],
            ["style", "font-size", '11px']
         ],
         "${symbolSelector}": [
            ["style", "height", '768px'],
            ["style", "width", '1024px']
         ],
         "${_QuestionPanelCorrect}": [
            ["style", "display", 'none']
         ],
         "${_answer}": [
            ["style", "top", '363px'],
            ["style", "height", '44px'],
            ["style", "width", '344px'],
            ["style", "left", '359px'],
            ["style", "font-size", '16px']
         ],
         "${_QuestionPanelWrong}": [
            ["style", "display", 'none'],
            ["style", "left", '0px'],
            ["style", "top", '0px']
         ],
         "${_num}": [
            ["style", "top", '361px'],
            ["style", "left", '310px'],
            ["style", "font-size", '39px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 2000,
         autoPlay: false,
         labels: {
            "Answer": 0,
            "Correct": 1000,
            "Wrong": 2000
         },
         timeline: [
            { id: "eid126", tween: [ "style", "${_QuestionPanelCorrect}", "display", 'block', { fromValue: 'none'}], position: 1000, duration: 0, easing: "easeInQuad" },
            { id: "eid211", tween: [ "style", "${_QuestionPanelCorrect}", "display", 'none', { fromValue: 'block'}], position: 2000, duration: 0, easing: "easeOutBounce" },
            { id: "eid125", tween: [ "style", "${_QuestionPanelWrong}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0, easing: "easeInQuad" },
            { id: "eid124", tween: [ "style", "${_QuestionPanelWrong}", "display", 'block', { fromValue: 'none'}], position: 2000, duration: 0, easing: "easeInQuad" }         ]
      }
   }
},
"QuestionScreen": {
   version: "1.0.0",
   minimumCompatibleVersion: "0.1.7",
   build: "1.0.0.185",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      rect: ['0px','10px','980px','650px','auto','auto'],
      id: 'Rectangle6',
      stroke: [0,'rgb(0, 0, 0)','none'],
      type: 'rect',
      fill: ['rgba(255,255,255,1.00)']
   },
   {
      id: 'DottedLine2',
      type: 'image',
      rect: ['0px','0','1024px','768px','auto','auto'],
      fill: ['rgba(0,0,0,0)','images/DottedLine22.svg','0px','0px']
   },
   {
      rect: ['254px','63px','726px','131px','auto','auto'],
      id: 'Rectangle5',
      stroke: [0,'rgb(0, 0, 0)','none'],
      type: 'rect',
      fill: ['rgba(151,201,75,1.00)'],
      c: [
      {
         id: 'questionmark',
         type: 'image',
         rect: ['2px','-17px','124px','165px','auto','auto'],
         fill: ['rgba(0,0,0,0)','images/questionmark.png','0px','0px']
      },
      {
         type: 'text',
         rect: ['-37px','45px','591px','175px','auto','auto'],
         id: 'question',
         text: 'TextBoxQuestion TextBoxQuestion TextBoxQuestion TextBoxQuestion TextBoxQuestion TextBoxQuestion TextBoxQuestion TextBoxQuestion TextBoxQuestion TextBoxQuestion TextBoxQuestion TextBoxQuestion ',
         align: 'left',
         font: ['Arial, Helvetica, sans-serif',30,'rgba(57,67,117,1.00)','400','none','normal']
      }]
   },
   {
      type: 'rect',
      id: 'answer1',
      cursor: ['pointer'],
      clip: ['rect(0px 1024pxpx 768pxpx 0px)'],
      rect: ['0','263','auto','auto','auto','auto']
   },
   {
      type: 'rect',
      id: 'answer2',
      cursor: ['pointer'],
      clip: ['rect(0px 1024pxpx 768pxpx 0px)'],
      rect: ['174','347','auto','auto','auto','auto']
   },
   {
      type: 'rect',
      id: 'answer3',
      cursor: ['pointer'],
      clip: ['rect(0px 1024pxpx 768pxpx 0px)'],
      rect: ['105','324','auto','auto','auto','auto']
   },
   {
      type: 'rect',
      id: 'answer4',
      cursor: ['pointer'],
      clip: ['rect(0px 1024pxpx 768pxpx 0px)'],
      rect: ['60','311','auto','auto','auto','auto']
   },
   {
      id: 'next_btn',
      type: 'rect',
      cursor: ['pointer'],
      rect: ['739','573','auto','auto','auto','auto']
   }],
   symbolInstances: [
   {
      id: 'answer4',
      symbolName: 'Answer'
   },
   {
      id: 'answer2',
      symbolName: 'Answer'
   },
   {
      id: 'answer3',
      symbolName: 'Answer'
   },
   {
      id: 'answer1',
      symbolName: 'Answer'
   },
   {
      id: 'next_btn',
      symbolName: 'btn_next'
   }   ]
   },
   states: {
      "Base State": {
         "${_DottedLine2}": [
            ["style", "left", '510px']
         ],
         "${_question}": [
            ["style", "top", '9px'],
            ["style", "width", '590px'],
            ["style", "height", '117px'],
            ["color", "color", 'rgba(255,255,255,1.00)'],
            ["style", "font-weight", '400'],
            ["style", "left", '126px'],
            ["style", "font-size", '24px']
         ],
         "${_answer1}": [
            ["style", "top", '302px'],
            ["style", "left", '-212px'],
            ["style", "clip", [348,787,418,235], {valueTemplate:'rect(@@0@@px @@1@@px @@2@@px @@3@@px)'} ],
            ["style", "cursor", 'pointer']
         ],
         "${_answer3}": [
            ["style", "top", '400px'],
            ["style", "left", '-212px'],
            ["style", "clip", [350,792,420,234], {valueTemplate:'rect(@@0@@px @@1@@px @@2@@px @@3@@px)'} ],
            ["style", "cursor", 'pointer']
         ],
         "${symbolSelector}": [
            ["style", "height", '660px'],
            ["style", "width", '980px']
         ],
         "${_Rectangle5}": [
            ["style", "top", '653px'],
            ["style", "height", '131px'],
            ["color", "background-color", 'rgba(151,201,75,1.00)'],
            ["style", "left", '254px'],
            ["style", "width", '726px']
         ],
         "${_next_btn}": [
            ["style", "top", '795.67px'],
            ["style", "left", '735px'],
            ["style", "cursor", 'pointer']
         ],
         "${_questionmark}": [
            ["style", "left", '2px'],
            ["style", "top", '-17px']
         ],
         "${_Rectangle6}": [
            ["color", "background-color", 'rgba(255,255,255,1.00)'],
            ["style", "height", '650px'],
            ["style", "top", '10px'],
            ["style", "left", '0px'],
            ["style", "width", '980px']
         ],
         "${_answer4}": [
            ["style", "top", '450px'],
            ["style", "left", '-212px'],
            ["style", "clip", [350,794,420,237], {valueTemplate:'rect(@@0@@px @@1@@px @@2@@px @@3@@px)'} ],
            ["style", "cursor", 'pointer']
         ],
         "${_answer2}": [
            ["style", "top", '350px'],
            ["style", "left", '-212px'],
            ["style", "clip", [349,800,421,230], {valueTemplate:'rect(@@0@@px @@1@@px @@2@@px @@3@@px)'} ],
            ["style", "cursor", 'pointer']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 4000,
         autoPlay: false,
         timeline: [
            { id: "eid254", tween: [ "style", "${_answer3}", "clip", [350,792,420,234], { valueTemplate: 'rect(@@0@@px @@1@@px @@2@@px @@3@@px)', fromValue: [350,792,420,234]}], position: 1750, duration: 0 },
            { id: "eid158", tween: [ "style", "${_answer3}", "left", '-212px', { fromValue: '-212px'}], position: 2250, duration: 0, easing: "easeInQuad" },
            { id: "eid181", tween: [ "style", "${_answer2}", "top", '-40px', { fromValue: '350px'}], position: 0, duration: 2250, easing: "easeOutBounce" },
            { id: "eid179", tween: [ "style", "${_answer4}", "top", '100px', { fromValue: '450px'}], position: 0, duration: 2250, easing: "easeOutBounce" },
            { id: "eid154", tween: [ "style", "${_answer2}", "left", '-212px', { fromValue: '-212px'}], position: 2250, duration: 0, easing: "easeInQuad" },
            { id: "eid180", tween: [ "style", "${_answer3}", "top", '30px', { fromValue: '400px'}], position: 0, duration: 2250, easing: "easeOutBounce" },
            { id: "eid192", tween: [ "style", "${_next_btn}", "left", '735px', { fromValue: '735px'}], position: 2250, duration: 0, easing: "easeOutBounce" },
            { id: "eid185", tween: [ "style", "${_DottedLine2}", "left", '0px', { fromValue: '510px'}], position: 0, duration: 2250, easing: "easeInQuad" },
            { id: "eid182", tween: [ "style", "${_answer1}", "top", '-108px', { fromValue: '302px'}], position: 0, duration: 2250, easing: "easeOutBounce" },
            { id: "eid261", tween: [ "style", "${_answer2}", "clip", [349,800,421,230], { valueTemplate: 'rect(@@0@@px @@1@@px @@2@@px @@3@@px)', fromValue: [349,800,421,230]}], position: 1750, duration: 0 },
            { id: "eid178", tween: [ "style", "${_Rectangle5}", "top", '63px', { fromValue: '653px'}], position: 0, duration: 1750, easing: "easeOutBounce" },
            { id: "eid160", tween: [ "style", "${_answer4}", "left", '-212px', { fromValue: '-212px'}], position: 2250, duration: 0, easing: "easeInQuad" },
            { id: "eid195", tween: [ "style", "${_next_btn}", "top", '506px', { fromValue: '795.67px'}], position: 2250, duration: 1250, easing: "easeInQuad" },
            { id: "eid196", tween: [ "style", "${_next_btn}", "top", '485.75px', { fromValue: '506px'}], position: 3500, duration: 250, easing: "easeInOutQuad" },
            { id: "eid197", tween: [ "style", "${_next_btn}", "top", '505.67px', { fromValue: '485.75px'}], position: 3750, duration: 250, easing: "easeOutQuad" },
            { id: "eid268", tween: [ "style", "${_answer1}", "clip", [348,787,418,235], { valueTemplate: 'rect(@@0@@px @@1@@px @@2@@px @@3@@px)', fromValue: [348,787,418,235]}], position: 1750, duration: 0 },
            { id: "eid148", tween: [ "style", "${_answer1}", "left", '-212px', { fromValue: '-212px'}], position: 2250, duration: 0, easing: "easeInQuad" },
            { id: "eid247", tween: [ "style", "${_answer4}", "clip", [350,794,420,237], { valueTemplate: 'rect(@@0@@px @@1@@px @@2@@px @@3@@px)', fromValue: [350,794,420,237]}], position: 1750, duration: 0 }         ]
      }
   }
},
"CheckBox": {
   version: "1.0.0",
   minimumCompatibleVersion: "0.1.7",
   build: "1.0.0.185",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      rect: ['0px','0px','36px','36px','auto','auto'],
      borderRadius: ['10px','10px','10px','10px'],
      id: 'RoundRect3',
      stroke: [0,'rgb(0, 0, 0)','none'],
      type: 'rect',
      fill: ['rgba(190,192,194,1.00)']
   },
   {
      type: 'rect',
      borderRadius: ['10px','10px','10px','10px'],
      rect: ['0px','0px','36px','36px','auto','auto'],
      id: 'RoundRect3Copy',
      stroke: [0,'rgb(0, 0, 0)','none'],
      display: 'none',
      fill: ['rgba(190,192,194,1.00)']
   },
   {
      type: 'rect',
      borderRadius: ['10px','10px','10px','10px'],
      rect: ['0px','0px','36px','36px','auto','auto'],
      id: 'RoundRect3Copy2',
      stroke: [0,'rgb(0, 0, 0)','none'],
      display: 'none',
      fill: ['rgba(190,192,194,1.00)']
   },
   {
      type: 'rect',
      borderRadius: ['10px','10px','10px','10px'],
      rect: ['0px','0px','36px','36px','auto','auto'],
      id: 'RoundRect3Copy3',
      stroke: [0,'rgb(0, 0, 0)','none'],
      display: 'none',
      fill: ['rgba(190,192,194,1.00)']
   },
   {
      type: 'image',
      display: 'none',
      rect: ['-4px','4px','43px','31px','auto','auto'],
      id: 'tick',
      fill: ['rgba(0,0,0,0)','images/tick.png','0px','0px']
   },
   {
      type: 'image',
      display: 'none',
      rect: ['0','0','36px','34px','auto','auto'],
      id: 'cross',
      fill: ['rgba(0,0,0,0)','images/cross.png','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_RoundRect3}": [
            ["color", "background-color", 'rgba(190,192,194,1.00)'],
            ["style", "left", '0px'],
            ["style", "top", '0px']
         ],
         "${_tick}": [
            ["style", "top", '4px'],
            ["style", "left", '-4px'],
            ["style", "display", 'none']
         ],
         "${_RoundRect3Copy}": [
            ["color", "background-color", 'rgba(45,163,220,1.00)'],
            ["style", "display", 'none'],
            ["style", "left", '0px'],
            ["style", "top", '0px']
         ],
         "${_cross}": [
            ["style", "display", 'none'],
            ["style", "left", '-1px']
         ],
         "${_RoundRect3Copy2}": [
            ["style", "display", 'none'],
            ["style", "top", '0px'],
            ["style", "left", '0px'],
            ["color", "background-color", 'rgba(61,139,65,1.00)']
         ],
         "${_RoundRect3Copy3}": [
            ["style", "display", 'none'],
            ["style", "top", '0px'],
            ["style", "left", '0px'],
            ["color", "background-color", 'rgba(237,28,36,1.00)']
         ],
         "${symbolSelector}": [
            ["style", "height", '36px'],
            ["style", "width", '36px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 3000,
         autoPlay: false,
         labels: {
            "Blank": 0,
            "Checked": 1000,
            "Correct": 2000,
            "Wrong": 3000
         },
         timeline: [
            { id: "eid170", tween: [ "style", "${_tick}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0, easing: "easeInQuad" },
            { id: "eid169", tween: [ "style", "${_tick}", "display", 'block', { fromValue: 'none'}], position: 1000, duration: 0, easing: "easeInQuad" },
            { id: "eid209", tween: [ "style", "${_tick}", "display", 'none', { fromValue: 'block'}], position: 3000, duration: 0, easing: "easeOutBounce" },
            { id: "eid175", tween: [ "color", "${_RoundRect3Copy}", "background-color", 'rgba(45,163,220,1.00)', { animationColorSpace: 'RGB', valueTemplate: undefined, fromValue: 'rgba(45,163,220,1.00)'}], position: 1000, duration: 0, easing: "easeInQuad" },
            { id: "eid206", tween: [ "color", "${_RoundRect3Copy3}", "background-color", 'rgba(237,28,36,1.00)', { animationColorSpace: 'RGB', valueTemplate: undefined, fromValue: 'rgba(237,28,36,1.00)'}], position: 3000, duration: 0, easing: "easeOutBounce" },
            { id: "eid208", tween: [ "style", "${_cross}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0, easing: "easeOutBounce" },
            { id: "eid207", tween: [ "style", "${_cross}", "display", 'block', { fromValue: 'none'}], position: 3000, duration: 0, easing: "easeOutBounce" },
            { id: "eid210", tween: [ "style", "${_cross}", "left", '-1px', { fromValue: '-1px'}], position: 3000, duration: 0, easing: "easeOutBounce" },
            { id: "eid204", tween: [ "style", "${_RoundRect3Copy3}", "display", 'none', { fromValue: 'none'}], position: 2000, duration: 0, easing: "easeInQuad" },
            { id: "eid205", tween: [ "style", "${_RoundRect3Copy3}", "display", 'block', { fromValue: 'none'}], position: 3000, duration: 0, easing: "easeInQuad" },
            { id: "eid202", tween: [ "color", "${_RoundRect3Copy2}", "background-color", 'rgba(61,139,65,1.00)', { animationColorSpace: 'RGB', valueTemplate: undefined, fromValue: 'rgba(61,139,65,1.00)'}], position: 2000, duration: 0, easing: "easeOutBounce" },
            { id: "eid201", tween: [ "style", "${_RoundRect3Copy2}", "display", 'block', { fromValue: 'none'}], position: 2000, duration: 0, easing: "easeInQuad" },
            { id: "eid173", tween: [ "style", "${_RoundRect3Copy}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0, easing: "easeInQuad" },
            { id: "eid174", tween: [ "style", "${_RoundRect3Copy}", "display", 'block', { fromValue: 'none'}], position: 1000, duration: 0, easing: "easeInQuad" }         ]
      }
   }
},
"btn_next": {
   version: "1.0.0",
   minimumCompatibleVersion: "0.1.7",
   build: "1.0.0.185",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      id: 'next_arrow',
      type: 'image',
      rect: ['0px','0px','62px','60px','auto','auto'],
      fill: ['rgba(0,0,0,0)','images/next_arrow.png','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_next_arrow}": [
            ["style", "left", '0px'],
            ["style", "top", '0px']
         ],
         "${symbolSelector}": [
            ["style", "height", '60px'],
            ["style", "width", '62px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 0,
         autoPlay: false,
         timeline: [
         ]
      }
   }
},
"btn_next_1": {
   version: "1.0.0",
   minimumCompatibleVersion: "0.1.7",
   build: "1.0.0.185",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      id: 'next_arrow',
      type: 'image',
      rect: ['0px','0px','62px','60px','auto','auto'],
      fill: ['rgba(0,0,0,0)','images/next_arrow2.png','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_next_arrow}": [
            ["style", "left", '0px'],
            ["style", "top", '0px']
         ],
         "${symbolSelector}": [
            ["style", "height", '60px'],
            ["style", "width", '62px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 0,
         autoPlay: false,
         timeline: [
         ]
      }
   }
},
"SetUpScreen": {
   version: "1.0.0",
   minimumCompatibleVersion: "0.1.7",
   build: "1.0.0.185",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      rect: ['0px','10px','980px','650px','auto','auto'],
      id: 'Rectangle6',
      stroke: [0,'rgb(0, 0, 0)','none'],
      type: 'rect',
      fill: ['rgba(255,255,255,1.00)']
   },
   {
      id: 'photo',
      type: 'image',
      rect: ['0px','0px','980px','650px','auto','auto'],
      fill: ['rgba(0,0,0,0)','images/setup_photo01.png','0px','0px']
   },
   {
      font: ['Arial, Helvetica, sans-serif',30,'rgba(57,67,117,1.00)','400','none','normal'],
      type: 'text',
      id: 'text1',
      text: 'TextBoxSetUp',
      align: 'left',
      rect: ['217px','108px','591px','175px','auto','auto']
   },
   {
      id: 'next_btn',
      type: 'rect',
      cursor: ['pointer'],
      rect: ['485','495','auto','auto','auto','auto']
   }],
   symbolInstances: [
   {
      id: 'next_btn',
      symbolName: 'btn_next_1'
   }   ]
   },
   states: {
      "Base State": {
         "${_text1}": [
            ["style", "top", '34.17px'],
            ["color", "color", 'rgba(57,67,117,1.00)'],
            ["style", "font-weight", '400'],
            ["style", "left", '998.9px'],
            ["style", "font-size", '30px']
         ],
         "${symbolSelector}": [
            ["style", "height", '660px'],
            ["style", "width", '980px']
         ],
         "${_next_btn}": [
            ["style", "top", '665px'],
            ["transform", "scaleY", '1'],
            ["style", "cursor", 'pointer'],
            ["transform", "scaleX", '1']
         ],
         "${_photo}": [
            ["style", "left", '-380px'],
            ["style", "top", '0px']
         ],
         "${_Rectangle6}": [
            ["color", "background-color", 'rgba(255,255,255,1.00)'],
            ["style", "height", '650px'],
            ["style", "top", '10px'],
            ["style", "left", '0px'],
            ["style", "width", '980px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 3500,
         autoPlay: false,
         timeline: [
            { id: "eid230", tween: [ "style", "${_text1}", "left", '220px', { fromValue: '998.9px'}], position: 750, duration: 750, easing: "easeOutBounce" },
            { id: "eid117", tween: [ "transform", "${_next_btn}", "scaleX", '1', { fromValue: '1'}], position: 2000, duration: 0, easing: "easeInQuad" },
            { id: "eid116", tween: [ "transform", "${_next_btn}", "scaleX", '1.2', { fromValue: '1'}], position: 2500, duration: 142, easing: "easeInQuad" },
            { id: "eid120", tween: [ "transform", "${_next_btn}", "scaleX", '1', { fromValue: '1.2'}], position: 2642, duration: 108, easing: "easeInQuad" },
            { id: "eid82", tween: [ "style", "${_next_btn}", "top", '495px', { fromValue: '665px'}], position: 2000, duration: 500, easing: "easeInQuad" },
            { id: "eid110", tween: [ "style", "${_next_btn}", "top", '495px', { fromValue: '495px'}], position: 2750, duration: 0, easing: "easeInQuad" },
            { id: "eid102", tween: [ "style", "${_next_btn}", "top", '495px', { fromValue: '495px'}], position: 3141, duration: 0, easing: "easeInQuad" },
            { id: "eid94", tween: [ "style", "${_next_btn}", "top", '495px', { fromValue: '495px'}], position: 3500, duration: 0, easing: "easeInQuad" },
            { id: "eid73", tween: [ "style", "${_photo}", "left", '0px', { fromValue: '-380px'}], position: 0, duration: 500 },
            { id: "eid76", tween: [ "style", "${_photo}", "left", '20px', { fromValue: '0px'}], position: 500, duration: 117, easing: "easeInOutQuad" },
            { id: "eid77", tween: [ "style", "${_photo}", "left", '0px', { fromValue: '20px'}], position: 617, duration: 133, easing: "easeInOutQuad" },
            { id: "eid115", tween: [ "transform", "${_next_btn}", "scaleY", '1', { fromValue: '1'}], position: 2000, duration: 0, easing: "easeInQuad" },
            { id: "eid118", tween: [ "transform", "${_next_btn}", "scaleY", '1.2', { fromValue: '1'}], position: 2500, duration: 142, easing: "easeInQuad" },
            { id: "eid121", tween: [ "transform", "${_next_btn}", "scaleY", '1', { fromValue: '1.2'}], position: 2642, duration: 108, easing: "easeInQuad" },
            { id: "eid80", tween: [ "style", "${_text1}", "top", '94.44px', { fromValue: '34.17px'}], position: 750, duration: 382, easing: "easeInQuad" }         ]
      }
   }
},
"content1_sym": {
   version: "1.0.0",
   minimumCompatibleVersion: "0.1.7",
   build: "1.0.0.185",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      display: 'none',
      type: 'rect',
      rect: ['490','341','auto','auto','auto','auto'],
      id: 'QuestionScreen'
   },
   {
      display: 'none',
      type: 'rect',
      rect: ['460','325','auto','auto','auto','auto'],
      id: 'CoachingScreen'
   },
   {
      display: 'none',
      type: 'rect',
      rect: ['490','309','auto','auto','auto','auto'],
      id: 'SetUpScreen'
   },
   {
      display: 'none',
      type: 'rect',
      rect: ['0','0','auto','auto','auto','auto'],
      id: 'MapInteractiveScreen'
   },
   {
      display: 'none',
      type: 'rect',
      rect: ['490','316','auto','auto','auto','auto'],
      id: 'IntroScreen'
   },
   {
      display: 'none',
      type: 'rect',
      rect: ['490','285','974px','637px','auto','auto'],
      id: 'Video2'
   }],
   symbolInstances: [
   {
      id: 'Video2',
      symbolName: 'Video'
   },
   {
      id: 'MapInteractiveScreen',
      symbolName: 'MapInteractiveScreen'
   },
   {
      id: 'IntroScreen',
      symbolName: 'IntroScreen'
   },
   {
      id: 'QuestionScreen',
      symbolName: 'QuestionScreen'
   },
   {
      id: 'CoachingScreen',
      symbolName: 'CoachingScreen'
   },
   {
      id: 'SetUpScreen',
      symbolName: 'SetUpScreen'
   }   ]
   },
   states: {
      "Base State": {
         "${_QuestionScreen}": [
            ["style", "top", '-10px'],
            ["style", "left", '0px'],
            ["style", "display", 'none']
         ],
         "${_CoachingScreen}": [
            ["style", "top", '0px'],
            ["style", "left", '-1px'],
            ["style", "display", 'none']
         ],
         "${symbolSelector}": [
            ["style", "height", '650px'],
            ["style", "width", '980px']
         ],
         "${_Video2}": [
            ["style", "top", '0.43px'],
            ["transform", "scaleY", '1'],
            ["style", "height", '560.16668701172px'],
            ["transform", "scaleX", '1'],
            ["style", "display", 'none'],
            ["style", "left", '2.18px'],
            ["style", "width", '929.81665039062px']
         ],
         "${_IntroScreen}": [
            ["style", "top", '-5px'],
            ["style", "left", '0px'],
            ["style", "display", 'none']
         ],
         "${_SetUpScreen}": [
            ["style", "top", '10.79px'],
            ["style", "left", '0px'],
            ["style", "display", 'none']
         ],
         "${_MapInteractiveScreen}": [
            ["style", "display", 'none']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 6750,
         autoPlay: true,
         labels: {
            "Question": 1000,
            "Coaching": 2000,
            "Animation": 3000,
            "Map": 4000,
            "VideoFrm": 5000,
            "TitleFrm": 6000
         },
         timeline: [
            { id: "eid165", tween: [ "style", "${_Video2}", "top", '32.25px', { fromValue: '0.43px'}], position: 5000, duration: 0, easing: "easeInQuad" },
            { id: "eid85", tween: [ "style", "${_QuestionScreen}", "left", '0px', { fromValue: '0px'}], position: 0, duration: 0, easing: "easeInOutBounce" },
            { id: "eid123", tween: [ "style", "${_MapInteractiveScreen}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
            { id: "eid122", tween: [ "style", "${_MapInteractiveScreen}", "display", 'block', { fromValue: 'none'}], position: 4000, duration: 0 },
            { id: "eid127", tween: [ "style", "${_MapInteractiveScreen}", "display", 'none', { fromValue: 'block'}], position: 4750, duration: 0 },
            { id: "eid103", tween: [ "style", "${_SetUpScreen}", "display", 'block', { fromValue: 'none'}], position: 3000, duration: 0, easing: "easeInOutBounce" },
            { id: "eid104", tween: [ "style", "${_SetUpScreen}", "display", 'none', { fromValue: 'block'}], position: 3750, duration: 0, easing: "easeInOutBounce" },
            { id: "eid101", tween: [ "style", "${_SetUpScreen}", "top", '10.79px', { fromValue: '10.79px'}], position: 3000, duration: 0, easing: "easeInOutBounce" },
            { id: "eid88", tween: [ "style", "${_QuestionScreen}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0, easing: "easeInOutBounce" },
            { id: "eid87", tween: [ "style", "${_QuestionScreen}", "display", 'block', { fromValue: 'none'}], position: 1000, duration: 0, easing: "easeInOutBounce" },
            { id: "eid89", tween: [ "style", "${_QuestionScreen}", "display", 'none', { fromValue: 'block'}], position: 1750, duration: 0, easing: "easeInOutBounce" },
            { id: "eid86", tween: [ "style", "${_QuestionScreen}", "top", '-10px', { fromValue: '-10px'}], position: 0, duration: 0, easing: "easeInOutBounce" },
            { id: "eid132", tween: [ "style", "${_IntroScreen}", "top", '-5px', { fromValue: '-5px'}], position: 6000, duration: 0 },
            { id: "eid98", tween: [ "style", "${_CoachingScreen}", "display", 'block', { fromValue: 'none'}], position: 2000, duration: 0, easing: "easeInOutBounce" },
            { id: "eid99", tween: [ "style", "${_CoachingScreen}", "display", 'none', { fromValue: 'block'}], position: 2750, duration: 0, easing: "easeInOutBounce" },
            { id: "eid100", tween: [ "style", "${_SetUpScreen}", "left", '0px', { fromValue: '0px'}], position: 3000, duration: 0, easing: "easeInOutBounce" },
            { id: "eid162", tween: [ "style", "${_Video2}", "left", '25.03px', { fromValue: '2.18px'}], position: 5000, duration: 0, easing: "easeInQuad" },
            { id: "eid105", tween: [ "style", "${_CoachingScreen}", "left", '-1px', { fromValue: '-1px'}], position: 2000, duration: 0, easing: "easeInOutBounce" },
            { id: "eid133", tween: [ "style", "${_IntroScreen}", "display", 'block', { fromValue: 'none'}], position: 6000, duration: 0 },
            { id: "eid134", tween: [ "style", "${_IntroScreen}", "display", 'none', { fromValue: 'block'}], position: 6750, duration: 0 },
            { id: "eid159", tween: [ "style", "${_Video2}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0, easing: "easeInQuad" },
            { id: "eid161", tween: [ "style", "${_Video2}", "display", 'block', { fromValue: 'none'}], position: 5000, duration: 0, easing: "easeInQuad" },
            { id: "eid149", tween: [ "style", "${_Video2}", "display", 'none', { fromValue: 'block'}], position: 5750, duration: 0, easing: "easeInQuad" },
            { id: "eid97", tween: [ "style", "${_CoachingScreen}", "top", '0px', { fromValue: '0px'}], position: 2000, duration: 0, easing: "easeInOutBounce" },
            { id: "eid131", tween: [ "style", "${_IntroScreen}", "left", '0px', { fromValue: '0px'}], position: 6000, duration: 0 },
            { id: "eid91", trigger: [ function executeSymbolFunction(e, data) { this._executeSymbolAction(e, data); }, ['play', '${_QuestionScreen}', [] ], ""], position: 1000 },
            { id: "eid106", trigger: [ function executeSymbolFunction(e, data) { this._executeSymbolAction(e, data); }, ['play', '${_CoachingScreen}', [] ], ""], position: 2000 },
            { id: "eid107", trigger: [ function executeSymbolFunction(e, data) { this._executeSymbolAction(e, data); }, ['play', '${_SetUpScreen}', [] ], ""], position: 3000 },
            { id: "eid224", trigger: [ function executeSymbolFunction(e, data) { this._executeSymbolAction(e, data); }, ['play', '${_MapInteractiveScreen}', [] ], ""], position: 4000 },
            { id: "eid225", trigger: [ function executeSymbolFunction(e, data) { this._executeSymbolAction(e, data); }, ['play', '${_Video2}', [] ], ""], position: 5000 },
            { id: "eid227", trigger: [ function executeSymbolFunction(e, data) { this._executeSymbolAction(e, data); }, ['stop', '${_Video2}', [] ], ""], position: 5750 },
            { id: "eid226", trigger: [ function executeSymbolFunction(e, data) { this._executeSymbolAction(e, data); }, ['play', '${_IntroScreen}', [] ], ""], position: 6000 }         ]
      }
   }
},
"MapInteractiveScreen": {
   version: "1.0.0",
   minimumCompatibleVersion: "0.1.7",
   build: "1.0.0.185",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      type: 'rect',
      id: 'Rectangle',
      stroke: [0,'rgb(0, 0, 0)','none'],
      rect: ['0px','0px','980px','650px','auto','auto'],
      fill: ['rgba(255,255,255,1)']
   },
   {
      rect: ['0px','30px','980px','650px','auto','auto'],
      transform: {},
      id: 'map0002',
      type: 'image',
      clip: ['rect(120px 483px 431px 154px)'],
      fill: ['rgba(0,0,0,0)','images/map0002.png','0px','0px']
   },
   {
      type: 'image',
      id: 'map0003',
      rect: ['0','30px','980px','650px','auto','auto'],
      clip: ['rect(154px 773px 366px 460px)'],
      fill: ['rgba(0,0,0,0)','images/map0003.png','0px','0px']
   },
   {
      type: 'image',
      id: 'map0004',
      rect: ['0','30px','980px','650px','auto','auto'],
      clip: ['rect(270px 642px 528px 394px)'],
      fill: ['rgba(0,0,0,0)','images/map0004.png','0px','0px']
   },
   {
      type: 'image',
      id: 'map0005',
      rect: ['0','30px','980px','650px','auto','auto'],
      clip: ['rect(141px 841px 314px 753px)'],
      fill: ['rgba(0,0,0,0)','images/map0005.png','0px','0px']
   },
   {
      type: 'image',
      id: 'map0006',
      rect: ['0px','30px','980px','650px','auto','auto'],
      clip: ['rect(281px 798px 533px 597px)'],
      fill: ['rgba(0,0,0,0)','images/map0006.png','0px','0px']
   },
   {
      rect: ['78px','76px','821px','71px','auto','auto'],
      id: 'message',
      text: 'Text goes here',
      font: ['Arial, Helvetica, sans-serif',24,'rgba(38,50,110,1.00)','normal','none',''],
      type: 'text'
   },
   {
      id: 'pin5',
      type: 'rect',
      rect: ['198','415','auto','auto','auto','auto']
   },
   {
      id: 'btn_next',
      type: 'rect',
      cursor: ['pointer'],
      rect: ['872','568','auto','auto','auto','auto']
   }],
   symbolInstances: [
   {
      id: 'btn_next',
      symbolName: 'btn_next'
   },
   {
      id: 'pin5',
      symbolName: 'MapPin'
   }   ]
   },
   states: {
      "Base State": {
         "${_map0003}": [
            ["style", "top", '30px'],
            ["style", "clip", [154,773,366,460], {valueTemplate:'rect(@@0@@px @@1@@px @@2@@px @@3@@px)'} ]
         ],
         "${_map0005}": [
            ["style", "top", '30px'],
            ["style", "clip", [141,841,314,753], {valueTemplate:'rect(@@0@@px @@1@@px @@2@@px @@3@@px)'} ]
         ],
         "${_map0004}": [
            ["style", "top", '30px'],
            ["style", "clip", [270,642,528,394], {valueTemplate:'rect(@@0@@px @@1@@px @@2@@px @@3@@px)'} ]
         ],
         "${_message}": [
            ["style", "width", '821.29998779297px'],
            ["color", "color", 'rgba(38,50,110,1.00)'],
            ["style", "height", '70.650001525879px'],
            ["style", "left", '78.03px'],
            ["style", "font-size", '24px']
         ],
         "${_map0002}": [
            ["style", "top", '30px'],
            ["style", "left", '0px'],
            ["style", "clip", [120,483,431,154], {valueTemplate:'rect(@@0@@px @@1@@px @@2@@px @@3@@px)'} ]
         ],
         "${_map0006}": [
            ["style", "top", '30px'],
            ["style", "clip", [281,798,533,597], {valueTemplate:'rect(@@0@@px @@1@@px @@2@@px @@3@@px)'} ]
         ],
         "${_Rectangle}": [
            ["style", "height", '650px'],
            ["style", "top", '0px'],
            ["style", "left", '0px'],
            ["style", "width", '980px']
         ],
         "${symbolSelector}": [
            ["style", "height", '650px'],
            ["style", "width", '980px']
         ],
         "${_btn_next}": [
            ["style", "top", '502px'],
            ["style", "left", '844.54px'],
            ["style", "cursor", 'pointer']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 1000,
         autoPlay: false,
         timeline: [
            { id: "eid291", tween: [ "style", "${_btn_next}", "left", '844.54px', { fromValue: '844.54px'}], position: 0, duration: 0, easing: "easeOutBounce" },
            { id: "eid292", tween: [ "style", "${_btn_next}", "top", '502px', { fromValue: '502px'}], position: 0, duration: 0, easing: "easeOutBounce" }         ]
      }
   }
},
"IntroScreen": {
   version: "1.0.0",
   minimumCompatibleVersion: "0.1.7",
   build: "1.0.0.185",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      type: 'rect',
      id: 'bg',
      stroke: [0,'rgb(0, 0, 0)','none'],
      rect: ['0px','10px','980px','650px','auto','auto'],
      fill: ['rgba(255,255,255,1.00)']
   },
   {
      id: 'titleImage',
      type: 'image',
      rect: ['0px','0px','980px','650px','auto','auto'],
      fill: ['rgba(0,0,0,0)','images/setup_photo01.png','0px','0px']
   },
   {
      type: 'text',
      rect: ['217px','108px','591px','175px','auto','auto'],
      id: 'title',
      text: 'TextBoxSetUp',
      align: 'left',
      font: ['Arial, Helvetica, sans-serif',30,'rgba(57,67,117,1.00)','400','none','normal']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_bg}": [
            ["color", "background-color", 'rgba(255,255,255,1.00)'],
            ["style", "height", '650px'],
            ["style", "top", '10px'],
            ["style", "left", '0px'],
            ["style", "width", '980px']
         ],
         "${_title}": [
            ["style", "top", '68px'],
            ["color", "color", 'rgba(57,67,117,1.00)'],
            ["style", "font-weight", '400'],
            ["style", "left", '1007px'],
            ["style", "font-size", '50px']
         ],
         "${_titleImage}": [
            ["style", "left", '-380px'],
            ["style", "top", '0px']
         ],
         "${symbolSelector}": [
            ["style", "height", '660px'],
            ["style", "width", '980px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 6500,
         autoPlay: false,
         timeline: [
            { id: "eid135", tween: [ "style", "${_title}", "font-size", '50px', { fromValue: '50px'}], position: 6500, duration: 0 },
            { id: "eid280", tween: [ "style", "${_title}", "left", '327px', { fromValue: '1007px'}], position: 750, duration: 382, easing: "easeInQuad" },
            { id: "eid73", tween: [ "style", "${_titleImage}", "left", '0px', { fromValue: '-380px'}], position: 0, duration: 500 },
            { id: "eid76", tween: [ "style", "${_titleImage}", "left", '20px', { fromValue: '0px'}], position: 500, duration: 117, easing: "easeInOutQuad" },
            { id: "eid77", tween: [ "style", "${_titleImage}", "left", '0px', { fromValue: '20px'}], position: 617, duration: 133, easing: "easeInOutQuad" }         ]
      }
   }
},
"Video": {
   version: "1.0.0",
   minimumCompatibleVersion: "0.1.7",
   build: "1.0.0.185",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: true,
   content: {
   dom: [
   {
      id: 'video',
      type: 'image',
      rect: ['0','0','930px','560px','auto','auto'],
      fill: ['rgba(0,0,0,0)','media/video1.png','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_video}": [
            ["style", "height", '560px'],
            ["style", "width", '930px']
         ],
         "${symbolSelector}": [
            ["style", "height", '560px'],
            ["style", "width", '930px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 1136,
         autoPlay: false,
         timeline: [
         ]
      }
   }
},
"dottedline": {
   version: "1.0.0",
   minimumCompatibleVersion: "0.1.7",
   build: "1.0.0.185",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      id: 'dottedline',
      type: 'image',
      rect: ['0px','0px','558px','2034px','auto','auto'],
      fill: ['rgba(0,0,0,0)','images/dottedline.png','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${symbolSelector}": [
            ["style", "height", '2034px'],
            ["style", "width", '558px']
         ],
         "${_dottedline}": [
            ["style", "left", '0.02px'],
            ["style", "top", '0px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 2500,
         autoPlay: false,
         timeline: [
            { id: "eid157", tween: [ "style", "${_dottedline}", "top", '-1425px', { fromValue: '0px'}], position: 0, duration: 2500 }         ]
      }
   }
},
"blob": {
   version: "1.0.0",
   minimumCompatibleVersion: "0.1.7",
   build: "1.0.0.185",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      rect: ['0px','0px','83px','80px','auto','auto'],
      borderRadius: ['50%','50%','50%','50%'],
      id: 'Ellipse',
      stroke: [0,'rgb(0, 0, 0)','none'],
      type: 'ellipse',
      fill: ['rgba(201,219,61,1.00)']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_Ellipse}": [
            ["style", "top", '0px'],
            ["transform", "scaleY", '0.1'],
            ["transform", "scaleX", '0.1'],
            ["style", "opacity", '0'],
            ["style", "left", '0px'],
            ["color", "background-color", 'rgba(201,219,61,1.00)']
         ],
         "${symbolSelector}": [
            ["style", "height", '80px'],
            ["style", "width", '83px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 2000,
         autoPlay: false,
         timeline: [
            { id: "eid142", tween: [ "style", "${_Ellipse}", "opacity", '1', { fromValue: '0'}], position: 0, duration: 1000 },
            { id: "eid144", tween: [ "style", "${_Ellipse}", "opacity", '0', { fromValue: '1'}], position: 1000, duration: 1000 },
            { id: "eid131", tween: [ "transform", "${_Ellipse}", "scaleX", '1', { fromValue: '0.1'}], position: 0, duration: 1000 },
            { id: "eid135", tween: [ "transform", "${_Ellipse}", "scaleX", '0.1', { fromValue: '1'}], position: 1000, duration: 1000 },
            { id: "eid132", tween: [ "transform", "${_Ellipse}", "scaleY", '1', { fromValue: '0.1'}], position: 0, duration: 1000 },
            { id: "eid136", tween: [ "transform", "${_Ellipse}", "scaleY", '0.1', { fromValue: '1'}], position: 1000, duration: 1000 }         ]
      }
   }
},
"MapPin": {
   version: "1.0.0",
   minimumCompatibleVersion: "0.1.7",
   build: "1.0.0.185",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      rect: ['0px','0px','146px','173px','auto','auto'],
      transform: {},
      id: 'pin',
      type: 'image',
      cursor: ['pointer'],
      fill: ['rgba(0,0,0,0)','images/map_pin.png','0px','0px']
   },
   {
      rect: ['32px','38px','70px','57px','auto','auto'],
      font: ['Arial, Helvetica, sans-serif',16,'rgba(0,0,0,1)','normal','none',''],
      align: 'center',
      id: 'text',
      text: 'Drag to the map',
      cursor: ['pointer'],
      type: 'text'
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_text}": [
            ["style", "top", '37.82px'],
            ["style", "text-align", 'center'],
            ["style", "font-size", '16px'],
            ["style", "height", '57.333374023438px'],
            ["style", "left", '31.6px'],
            ["style", "cursor", 'pointer'],
            ["style", "width", '69.983329772949px']
         ],
         "${_pin}": [
            ["style", "top", '0px'],
            ["transform", "scaleY", '1'],
            ["transform", "scaleX", '1'],
            ["style", "left", '0px'],
            ["style", "cursor", 'pointer']
         ],
         "${symbolSelector}": [
            ["style", "height", '173px'],
            ["style", "width", '146px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 0,
         autoPlay: true,
         timeline: [
         ]
      }
   }
}
};


Edge.registerCompositionDefn(compId, symbols, fonts, resources);

/**
 * Adobe Edge DOM Ready Event Handler
 */
$(window).ready(function() {
     Edge.launchComposition(compId);
});
})(jQuery, AdobeEdge, "EDGE-5190220");
